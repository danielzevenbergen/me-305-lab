'''!@file       taskEncoder.py
    @brief      This file is responsible for updating the encoder object at
                regular intervals and otherwise interfacing with the encoder
    @details    This task has a certain period and is run once per period.
                Each time it runs, the encoder either updates, zeros, returns
                its position, returns the most recent delta, or starts or ends
                data collection. When data is being collected, a flag within
                the task "datarecord" is True, otherwise the flag is False.
                Data will record until the preallocated arrays are full
                (data recording timeout) or until the sFlag shared variable
                is False (this is triggered False by the User Task).
    @image      html EncoderTask_State_Transition_Diagram.png "Encoder Task Finite State Machine" width=960 height=540
    @author     Daniel Zevenbergen
    @date       02/02/2022
'''
import utime
from micropython import const
import array

def Etask(encoder, zFlag, pFlag, dFlag, gFlag, sFlag, pos, delta, timeData, positionData, deltaData, timeout, period):
    '''!@brief      This function is responsible for updating and otherwise
                    interfacing with the encoder object.
        @details    This function is called by main and runs every [period]
                    microseconds. The function works by checking each of the
                    shared "flag" variables to indicate whether it should
                    change its state. zFlag indicates that the encoder should
                    be zeroed (task must switch to "Zero" state), pFlag
                    indicates that the shared position variable "pos" should be
                    updated, dFlag indicates that the shared delta variable
                    "delta" should be updated, gFlag indicates that data
                    recording should begin, and sFlag indicates that data
                    recording should be terminated. The shared variables
                    timeData, positionData, and deltaData are for passing
                    encoder data back to the User task after recording is
                    complete. The shared variable timeout can be set to True to
                    indicate to the User task that data collection has been
                    halted due to timeout.
        @param      encoder This parameter is the encoder object to be
                    updated and interfaced with.
        @param      zFlag This is the shared variable which, when set to True,
                    indicates to the Encoder task that the encoder should be
                    zeroed.
        @param      pFlag This is the shared variable which, when set to True,
                    indicates to the Encoder task that the shared variable
                    pos should be updated with the most recent encoder
                    position.
        @param      dFlag This is the shared variable which, when set to True,
                    indicates to the Encoder task that the shared variable
                    delta should be updated with the most recent encoder
                    delta.
        @param      gFlag This is the shared variable which, when set to True,
                    indicates to the Encoder task that data collection should
                    begin.
        @param      sFlag This is the shared variable which, when set to True,
                    indicates to the Encoder task that data collection should
                    terminate.
        @param      pos This shared variable is used to pass the encoder
                    position from the Encoder task to the User task.
        @param      delta This shared variable is used to pass the encoder
                    delta from the Encoder task to the User task.
        @param      timeData This shared variable is used to pass an array of
                    time data from the Encoder task to the User task.
        @param      positionData This shared variable is used to pass an array 
                    of position data from the Encoder task to the User task.
        @param      deltaData This shared variable is used to pass an array of
                    delta data from the Encoder task to the User task.
        @param      timeout This shared variable is used to indicate to the
                    User task that data collection has timed out.
        @param      period This parameter, in microseconds, indicates to the
                    Encoder task how often it is supposed to run.
'''
    S0_INIT = const(0)
    S1_UPDATE = const(1)
    S2_ZERO = const(2)
    S3_POSITION = const(3)
    S4_DELTA = const(4)
    S5_DATASTART = const(5)
    S6_DATAEND = const(6)
    
    state = S0_INIT
    ##@brief    This variable is True if data recording is in progress
    datarecord = False
    ##@brief    This variable indicates the number of data points that may
    #           be recorded before data collection times out.
    datapoints = 3001
    ##@brief    This variable is the current index of the data collection
    index = 0 # index at which to place next data point
    ##@brief    This array stores time data during data collection
    timearray = array.array('l', datapoints*[0])
    ##@brief    This array stores position data during data collection
    positionarray = array.array('l', datapoints*[0])
    ##@brief    This array stores delta data during data collection
    deltaarray = array.array('l', datapoints*[0])
    ##@brief    Indicates the time at which the Encoder task was first run
    starttime = utime.ticks_us()
    ##@brief    Indicates the time at which data collection began
    datastarttime = starttime
    ##@brief    Indicates the next time at which the Encoder task must run
    nexttime = starttime
    while True:
        currenttime = utime.ticks_us() # indicates current time
        if utime.ticks_diff(nexttime, currenttime) <= 0: # check if it's time to run the encoder task
            nexttime = utime.ticks_add(nexttime, period) # update nexttime
            
            # This conditional records data every time the task runs if data recording is in progress
            if datarecord and index < datapoints: 
                timearray[index] = utime.ticks_diff(currenttime, datastarttime)
                positionarray[index] = encoder.getposition()
                deltaarray[index] = encoder.get_delta()
                index += 1 # increment index
            # This conditional checks for data collection timeout
            elif index >= datapoints:
                timeout.write(True)
                sFlag.write(True)
            
            # This set of conditionals checks for the various flags and sets
            # the state accordingly.
            if zFlag.read(): # conditionals to check flags for state activation
                state = S2_ZERO
            elif pFlag.read():
                state = S3_POSITION
            elif dFlag.read():
                state = S4_DELTA
            elif gFlag.read():
                state = S5_DATASTART
            elif sFlag.read():
                state = S6_DATAEND
            
            # This set of conditionals represent the various states of the
            # encoder task
            if state == S0_INIT: # state actions
                zFlag.write(False)
                pFlag.write(False)
                dFlag.write(False)
                gFlag.write(False)
                sFlag.write(False)
                state = S1_UPDATE
                yield None
            elif state == S1_UPDATE:
                encoder.update()
                yield None
            elif state == S2_ZERO:
                encoder.zero()
                state = S1_UPDATE
                zFlag.write(False)
                yield None
            elif state == S3_POSITION:
                pos.write(encoder.getposition())
                state = S1_UPDATE
                pFlag.write(False)
                yield None
            elif state == S4_DELTA:
                delta.write(encoder.get_delta())
                state = S1_UPDATE
                dFlag.write(False)
                yield None
            elif state == S5_DATASTART:
                datarecord = True
                datastarttime = utime.ticks_us()
                timearray = array.array('l', datapoints*[0])
                positionarray = array.array('l', datapoints*[0])
                deltaarray = array.array('l', datapoints*[0])
                state = S1_UPDATE
                gFlag.write(False)
                yield None
            elif state == S6_DATAEND:
                datarecord = False
                state = S1_UPDATE
                sFlag.write(False)
                timeData.write(timearray[0:index-1])
                positionData.write(positionarray[0:index-1])
                deltaData.write(deltaarray[0:index-1])
                index = 0
                yield None
        else:
            yield None
        