'''!@file       main.py
    @brief      This file is the main file for Lab 2. It creates a simple
                user interface and encoder object to allow the user to interact
                with an encoder via PuTTy
    @details    This file creates an encoder object using the encoder pins and
                timer on the STM32 Nucleo board and continually runs the
                user interface task (responsible for reading and managing user
                inputs) and the encoder task (responsible for updating and
                otherwise interacting with the encoder object). This file
                initializes all the variables required for the user task and the
                encoder task to run properly.
    @image      html Encoder_Position_vs_Time.png "Plot of encoder position data versus time" width=1127 height=750
    @image      html "Lab 2 Task Diagram.png" "Task Diagram for Lab 2" width=960
    @author     Daniel Zevenbergen
    @date       02/02/2022
'''
import shares
import utime
import pyb
import encoder
import taskEncoder
import taskUser

if __name__ == '__main__':
    pinENCA = pyb.Pin(pyb.Pin.board.PB6)
    pinENCB = pyb.Pin(pyb.Pin.board.PB7)
    timer = pyb.Timer(4, prescaler=0, period=65535)
    timerch1 = timer.channel(1, pyb.Timer.ENC_AB, pin=pinENCA)
    timerch2 = timer.channel(2, pyb.Timer.ENC_AB, pin=pinENCB)
    myEncoder = encoder.Encoder(pinENCA, pinENCB, timer, 0)
    zFlag = shares.Share(False)
    pFlag = shares.Share(False)
    dFlag = shares.Share(False)
    gFlag = shares.Share(False)
    sFlag = shares.Share(False)
    pos = shares.Share(0)
    delta = shares.Share(0)
    timeData = shares.Share()
    positionData = shares.Share()
    deltaData = shares.Share()
    timeout = shares.Share(False)
    
    Userfreq = 100 # User task frequency in Hz
    EncoderFreq = 100 # Encoder task frequency in Hz
    Userperiod = 1_000_000//Userfreq # User task period in us
    Encoderperiod = 1_000_000//EncoderFreq # Encoder task period in us
    serport = pyb.USB_VCP()
    
    task_Encoder = taskEncoder.Etask(myEncoder, zFlag, pFlag, dFlag, gFlag, sFlag, pos, delta, timeData, positionData, deltaData, timeout, Encoderperiod)
    task_User = taskUser.Utask(serport, zFlag, pFlag, dFlag, gFlag, sFlag, pos, delta, timeData, positionData, deltaData, timeout, Userperiod)
    tasklist = [task_Encoder, task_User]
    
    while True:
        try:
            for task in tasklist:
                next(task) 
        except KeyboardInterrupt:
            break