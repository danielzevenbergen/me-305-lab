'''!@file       taskUser.py
    @brief      This file is responsible for taking and handling user inputs
                and presenting data to the user.
    @details    This task has a certain period and is run once per period.
                Each time it runs, it checks for a user input via the USB
                virtual comport and processes the input accordingly. An input
                of z, p, d, g, or s (capital or lowercase) will result in
                the task raising the corresponding flag, which notifies the
                encoder task that some action is required. If any other input
                is entered, the start menu is printed, which contains all
                the user commands.
    @image      html UserTask_State_Transition_Diagram.png "User Task Finite State Machine" width=960 height=540
    @author     Daniel Zevenbergen
    @date       02/02/2022
'''

import utime
from micropython import const


def Utask(serport, zFlag, pFlag, dFlag, gFlag, sFlag, pos, delta, timeData, positionData, deltaData, timeout, period):
    '''!@brief      This function handles user inputs and raises flags to
                    prompt the encoder task to perform actions accordingly.
        @details    This function is called by main and runs every [period]
                    microseconds. In its usual state "S1_WAIT", the task checks
                    the serial port for user inputs and, if any are present,
                    reads and decodes the input. It will set the state to the
                    proper state (ZERO, POSITION, DELTA, DATASTART, or DATAEND)
                    depending on the user input. As it transitions to any state 
                    other than WAIT, the task raises a shared variable flag
                    (zFlag, pFlag, dFlag, gFlag, or sFlag) to indicate to the
                    encoder task that some action must be completed. Once the
                    action has been completed, the encoder task will lower the
                    flag, prompting the user task to return to the "WAIT" state.
                    The task also checks if the shared variable "timeout" has
                    been set to True, in which case the state will be set to
                    DATAEND to end data collection and print the data to
                    PuTTy.
        @param      serport This parameter is the serial port from which the
                    user task will read user input characters.
        @param      zFlag This is the shared variable which, when set to True,
                    indicates to the Encoder task that the encoder should be
                    zeroed.
        @param      pFlag This is the shared variable which, when set to True,
                    indicates to the Encoder task that the shared variable
                    pos should be updated with the most recent encoder
                    position.
        @param      dFlag This is the shared variable which, when set to True,
                    indicates to the Encoder task that the shared variable
                    delta should be updated with the most recent encoder
                    delta.
        @param      gFlag This is the shared variable which, when set to True,
                    indicates to the Encoder task that data collection should
                    begin.
        @param      sFlag This is the shared variable which, when set to True,
                    indicates to the Encoder task that data collection should
                    terminate.
        @param      pos This shared variable is used to pass the encoder
                    position from the Encoder task to the User task.
        @param      delta This shared variable is used to pass the encoder
                    delta from the Encoder task to the User task.
        @param      timeData This shared variable is used to pass an array of
                    time data from the Encoder task to the User task.
        @param      positionData This shared variable is used to pass an array 
                    of position data from the Encoder task to the User task.
        @param      deltaData This shared variable is used to pass an array of
                    delta data from the Encoder task to the User task.
        @param      timeout This shared variable is used to indicate to the
                    User task that data collection has timed out.
        @param      period This parameter, in microseconds, indicates to the
                    User task how often it is supposed to run.
    '''
    S0_INIT = const(0)
    S1_WAIT = const(1)
    S2_ZERO = const(2)
    S3_POSITION = const(3)
    S4_DELTA = const(4)
    S5_DATASTART = const(5)
    S6_DATAEND = const(6)
    
    state = S0_INIT
    starttime = utime.ticks_us()
    nexttime = starttime
    while True:
        currenttime = utime.ticks_us()
        if utime.ticks_diff(nexttime, currenttime) <= 0: # check if it's time to run the user task
            nexttime = utime.ticks_add(nexttime, period)
            
            # This conditional checks if the current state is S0_INIT and prints
            # the menu if it is
            if state == S0_INIT:
                print("Welcome to the program!")
                printmenu()
                state = S1_WAIT
                yield None
            
            # This conditional checks if the current state is S1_WAIT
            elif state == S1_WAIT:
                
                # This conditional checks if there are any characters waiting
                # to be read in the serport and, if so, decodes them and checks
                # if they are one of the user inputs. If they are, the state
                # is set accordingly. If not, the menu is printed again.
                if serport.any():
                    charIn = serport.read(1).decode()
                    if charIn in {'z', 'Z'}:
                        zFlag.write(True)
                        print('Zeroing...')
                        state = S2_ZERO
                    elif charIn in {'p', 'P'}:
                        pFlag.write(True)
                        print('Printing position...')
                        state = S3_POSITION
                    elif charIn in {'d', 'D'}:
                        dFlag.write(True)
                        print('Printing delta...')
                        state = S4_DELTA
                    elif charIn in {'g', 'G'}:
                        gFlag.write(True)
                        print('Starting data collection')
                        state = S5_DATASTART
                    elif charIn in {'s', 'S'}:
                        sFlag.write(True)
                        print('Terminating data collection')
                        state = S6_DATAEND
                    else:
                        print(f'You entered {charIn}.')
                        printmenu()
                # This conditional checks for timeout and sets the state to
                # DATAEND if timeout has occured.
                elif timeout.read():
                    sFlag.write(True)
                    timeout.write(False)
                    print('Data collection timeout')
                    state = S6_DATAEND
                yield None
            
            # The following set of conditionals check the current state of the
            # user task and raise the proper flag before resetting the state
            # to WAIT
            elif state == S2_ZERO:
                if not zFlag.read():
                    state = S1_WAIT
                    print ('Zeroed!')
                yield None
            elif state == S3_POSITION:
                if not pFlag.read():
                    state = S1_WAIT
                    print (f'Position = {pos.read()}')
                yield None
            elif state == S4_DELTA:
                if not dFlag.read():
                    state = S1_WAIT
                    print (f'Delta = {delta.read()}')
                yield None
            elif state == S5_DATASTART:
                if not gFlag.read():
                    state = S1_WAIT
                    print ('Data collection started')
                yield None
            elif state == S6_DATAEND:
                if not sFlag.read():
                    state = S1_WAIT
                    print ('Data Collection terminated')
                    for index in range(len(timeData.read())):
                        print(f'{timeData.read()[index]}, {positionData.read()[index]}, {deltaData.read()[index]}')
                        yield None
                yield None
        else:
            yield None
    
    
def printmenu():
    '''!@brief      This task prints a menu to PuTTy informing the user of the
                    commands they may use.
    '''
    print('+-----------------------------------------------------------------------+')
    print('|--Press z or Z to zero encoder-----------------------------------------|')
    print('|--Press p or P to print encoder position-------------------------------|')
    print('|--Press d or D to print encoder delta----------------------------------|')
    print('|--Press g or G to collect 30 seconds of encoder data and print as csv--|')
    print('|--Press s or S to end data collection early----------------------------|')
    print('+-----------------------------------------------------------------------+')