'''!@file       taskEncoder.py
    @brief      This file is responsible for updating the encoder object at
                regular intervals and otherwise interfacing with the encoder
    @details    This task has a certain period and is run once per period.
                Each time it runs, the encoder either updates, zeros, returns
                its position, returns the most recent delta, or starts or ends
                data collection. When data is being collected, a flag within
                the task "datarecord" is True, otherwise the flag is False.
                Data will record until the preallocated arrays are full
                (data recording timeout) or until the sFlag shared variable
                is False (this is triggered False by the User Task).
    @image      html "Lab 3 Encoder Task FSM.png" "Lab 3 Encoder Task Finite State Machine" width=960 height=540
    @author     Daniel Zevenbergen
    @date       02/02/2022
'''
import utime
from micropython import const

def Etask(encoder, zFlag, pos, delta, velocity, period):
    '''!@brief      This function is responsible for updating and otherwise
                    interfacing with the encoder object.
        @details    This function is called by main and runs every [period]
                    microseconds. The function works by checking each of the
                    shared "flag" variables to indicate whether it should
                    change its state. zFlag indicates that the encoder should
                    be zeroed (task must switch to "Zero" state), pFlag
                    indicates that the shared position variable "pos" should be
                    updated, dFlag indicates that the shared delta variable
                    "delta" should be updated, gFlag indicates that data
                    recording should begin, and sFlag indicates that data
                    recording should be terminated. The shared variables
                    timeData, positionData, and deltaData are for passing
                    encoder data back to the User task after recording is
                    complete. The shared variable timeout can be set to True to
                    indicate to the User task that data collection has been
                    halted due to timeout.
        @param      encoder This parameter is the encoder object to be
                    updated and interfaced with.
        @param      zFlag This is the shared variable which, when set to True,
                    indicates to the Encoder task that the encoder should be
                    zeroed.
        @param      pos This shared variable is used to pass the encoder
                    position from the Encoder task to the User task.
        @param      delta This shared variable is used to pass the encoder
                    delta from the Encoder task to the User task.
        @param      velocity This shared variable is used to pass the encoder
                    velocity from the encoder task to the user task.
        @param      period This parameter, in microseconds, indicates to the
                    Encoder task how often it is supposed to run.
'''
    S0_INIT = const(0)
    S1_UPDATE = const(1)
    
    state = S0_INIT
    ##@brief    Indicates the time at which the Encoder task was first run
    starttime = utime.ticks_us()
    ##@brief    Indicates the next time at which the Encoder task must run
    lasttime = starttime
    nexttime = starttime
    while True:
        currenttime = utime.ticks_us() # indicates current time
        if utime.ticks_diff(nexttime, currenttime) <= 0: # check if it's time to run the encoder task
            nexttime = utime.ticks_add(nexttime, period) # update nexttime
            
            # This set of conditionals represent the various states of the
            # encoder task
            if state == S0_INIT: # state actions
                zFlag.write(False)
                state = S1_UPDATE
                yield None
            elif state == S1_UPDATE:
                if zFlag.read():
                    encoder.zero()
                    zFlag.write(False)
                else:
                    encoder.update()
                    pos.write(encoder.getposition())
                    delta.write(encoder.get_delta())
                    deltatime = utime.ticks_diff(currenttime, lasttime)
                    velocity.write(encoder.get_delta()/deltatime*2*3.14159265/4000*1_000_000)
                yield None
            lasttime = currenttime # save currenttime for velocity calculation
        else:
            yield None
        