'''!@file       DRV8847.py
    @brief      This file creates an object to interface with the DRV8847
                motor driver on the STM32 Nucleo board.
    @author     Daniel Zevenbergen
    @date       02/10/2022
'''
from motor import Motor
from pyb import Pin, Timer, ExtInt
from utime import sleep_us

class DRV8847:
    '''!@brief      Motor driver class for the DRV8847 motor driver on the
                    STM32 Nucleo board
        @details    Objects of this class can be created to interface with
                    a Texas Instruments DRV8847 motor driver. the constructor
                    specifies which timer number and which pins the DRV8847 is
                    connected to.
    '''
    
    def __init__(self, timer_num, Pin_nSleep, Pin_nFault, Pin_IN1, Pin_IN2, Pin_IN3, Pin_IN4):
        '''!@brief      Initializes a DRV8847 object
            @details    This constructor accepts as arguments a timer number,
                        fault pins, sleep pins, and four input pins. It then
                        creates the timer object that will be used for motor
                        control using the given timer number. An external
                        interrupt is also defined, which triggers when the
                        fault pin is pulled low by the motor driver.
            @param      timer_num The timer number connected to the DRV8847
                        on the board
            @param      Pin_nSleep The pin on the board connected to the DRV8847's
                        nSleep pin
            @param      Pin_nFault The pin on the board connected to the DRV8847's
                        nFault pin
            @param      Pin_IN1 The pin on the board connected to the DRV8847's
                        INPUT 1 pin
            @param      Pin_IN2 The pin on the board connected to the DRV8847's
                        INPUT 2 pin
            @param      Pin_IN3 The pin on the board connected to the DRV8847's
                        INPUT 3 pin
            @param      Pin_IN4 The pin on the board connected to the DRV8847's
                        INPUT 4 pin
        '''
        self.timer_PWM = Timer(timer_num, freq=20000)
        self.Pin_nSleep = Pin_nSleep
        self.Pin_nFault = Pin_nFault
        self.Pin_IN1 = Pin_IN1
        self.Pin_IN2 = Pin_IN2
        self.Pin_IN3 = Pin_IN3
        self.Pin_IN4 = Pin_IN4
        
        self.FaultInt = ExtInt(self.Pin_nFault, mode=ExtInt.IRQ_FALLING, pull=Pin.PULL_NONE, callback=self.fault_cb)
    
    def enable(self):
        '''!@brief      Activates the DRV8847 (takes it out of sleep mode) by
                        setting the nSleep pin to logic high.
        '''
        self.FaultInt.disable()
        self.Pin_nSleep.on()
        sleep_us(50)
        self.FaultInt.enable()
    
    def disable(self):
        '''!@brief      Deactivates the DRV8847 (puts it in sleep mode) by
                        setting the nSleep pin to logic low.
        '''
        self.Pin_nSleep.off()
    
    def fault_cb(self, IRQ_src):
        '''!@brief      Handles faults thrown by the DRV8847
            @details    When the external interrupt defined in the constructor
                        function is tripped (nFault pin is pulled low), this
                        function is called. This function disables the motor
                        driver and prints a message to the user indicating
                        that a motor driver fault has occured.
            @param      IRQ_src The source of the interrupt request
        '''
        self.disable()
        print('Motor driver fault')
    
    def motor(self, motor_num):
        '''!@brief      Creates a motor object for a DC motor connected to
                        the DRV8847
            @param      motor_num The motor number wo be created. Must be either
                        1 or 2.
        '''
        if motor_num==1:
            timch1=self.timer_PWM.channel(1, Timer.PWM_INVERTED, pin=self.Pin_IN1)
            timch2=self.timer_PWM.channel(2, Timer.PWM_INVERTED, pin=self.Pin_IN2)
        elif motor_num==2:
            timch1=self.timer_PWM.channel(3, Timer.PWM_INVERTED, pin=self.Pin_IN3)
            timch2=self.timer_PWM.channel(4, Timer.PWM_INVERTED, pin=self.Pin_IN4)
        else:
            print('invalid motor_num; must be either 1 or 2')
        return Motor(self.timer_PWM, timch1, timch2)
    
if __name__ == '__main__':
    nSleep = Pin(Pin.board.PA15, mode=Pin.OUT)
    nFault = Pin(Pin.board.PB2, mode=Pin.OPEN_DRAIN)
    IN1 = Pin(Pin.board.PB4, mode=Pin.OUT)
    IN2 = Pin(Pin.board.PB5, mode=Pin.OUT)
    IN3 = Pin(Pin.board.PB0, mode=Pin.OUT)
    IN4 = Pin(Pin.board.PB1, mode=Pin.OUT)
    
    motor_drv = DRV8847(3, nSleep, nFault, IN1, IN2, IN3, IN4)
    motor_1 = motor_drv.motor(1)
    motor_2 = motor_drv.motor(2)
    
    motor_drv.enable()
    
    motor_1.set_duty(50)
    motor_2.set_duty(25)
    
    
    