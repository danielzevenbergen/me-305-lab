'''!@file       encoder.py
    @brief      This file contains the Encoder class which is used to create
                encoder objects. These objects are used to track encoder
                position and otherwise interface with the encoder.
    @author     Daniel Zevenbergen
    @date       02/16/2022
'''

class Encoder:
    '''!@brief      The encoder class is a driver for an encoder object. It can
                    update the position of the encoder, zero the encoder, and
                    return the current encoder position.
        @details    The encoder class interfaces with two encoder pins on the
                    board and a hardware timer which counts the encoder ticks.
                    This class is meant for interfacing with incremental
                    encoders. Each encoder object is initialized with a certain
                    position "startpos," which can be zero or any arbitrary
                    integer value.
    '''
    def __init__(self, pin1, pin2, timer, startpos):
        '''!@brief      Initialize an Encoder object
            @details    The __init__ method takes as inputs two pin objects 
                        representing physical pins on the board. These should 
                        be the pins the encoder is connected to. It also takes 
                        as an input a timer object representing a physical
                        timer on the board. This should be the timer
                        responsible for counting the encoder ticks. Finally,
                        it takes as an input the start position of the
                        encoder, which should be an integer.
            @param      pin1 This parameter is a pin object representing one
                        of the two physical pins to which the encoder is 
                        connected.
            @param      pin2 This parameter is a pin object representing the other
                        of the two physical pins to which the encoder is 
                        connected.
            @param      timer This parameter is a timer object representing
                        the physical timer responsible for counting
                        encoder ticks. The timer should have a prescaler of 0
                        so as to count all the encoder ticks, a period equal to
                        the ARV-1 of the timer, and its channels should be
                        configured in encoder mode using the proper pins.
            @param      startpos This parameter is an integer representing the
                        starting position of the encoder.
            
        '''
        self.pin1 = pin1
        self.pin2 = pin2
        self.timer = timer
        self.startpos = startpos
        self.period = self.timer.period()
        self.halfperiod = (self.period+1)//2
        self.position = startpos
        self.lastcount = self.timer.counter()
        
    def update(self):
        '''!@brief      Updates the position of the encoder, correcting for timer
                        rollover.
            @details    This method first computes the difference between the
                        most recently read timer count and the last timer count.
                        Timer rollover is detected by checking if the difference
                        is less than -1* half the timer period (upward rollover), 
                        or if the difference is greater than half the timer period 
                        (downward rollover). If neither of those evaluate to True,
                        then the timer has not rolled over and the change in the
                        encoder position is just the difference between the last
                        two timer counts. This method takes no parameters
                        and returns no values, only updates the object's position
                        and delta attributes.
        '''
        self.currentcount = self.timer.counter()
        self.diff = self.currentcount - self.lastcount
        if self.diff < (-1*self.halfperiod): # if statement for upward rollover
            self.delta = self.currentcount - self.lastcount + (self.period+1)
        elif self.diff > self.halfperiod: # if statement for downward rollover
            self.delta = self.currentcount - self.lastcount - (self.period+1)
        else:
            self.delta = self.currentcount - self.lastcount
        self.position+=self.delta
        self.lastcount = self.currentcount
        
    def getposition(self):
        '''!@brief     Returns the current position of the encoder
        '''
        return self.position
    
    def zero(self):
        '''!@brief     Sets the encoder position to zero
        '''
        self.position = 0
        
    def get_delta(self):
        '''!@brief     Returns the delta between the last two calls of the
                        update function
        '''
        return self.delta
    
    def set(self, value):
        '''!@brief      Sets the encoder position to a value
            @param      value This parameter is the value to which the encoder
                        position will be set.
        '''
        self.position = value