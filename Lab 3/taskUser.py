'''!@file       taskUser.py
    @brief      This file is responsible for taking and handling user inputs
                and presenting data to the user.
    @details    This task has a certain period and is run once per period.
                Each time it runs, it checks for a user input via the USB
                virtual comport and processes the input accordingly. An input
                of z, p, d, g, or s (capital or lowercase) will result in
                the task raising the corresponding flag, which notifies the
                encoder task that some action is required. If any other input
                is entered, the start menu is printed, which contains all
                the user commands.
    @image      html "Lab 3 User Task FSM.png" "Lab 3 User Task Finite State Machine" width=960 height=540
    @author     Daniel Zevenbergen
    @date       02/02/2022
'''

import utime
from micropython import const
import gc


def Utask(serport, zFlag, dataFlag, pos, delta, velocity, dutycycle1, dutycycle2, Driver, datarecord, timeData, positionData, deltaData, timeout, finalindex, period):
    '''!@brief      This function handles user inputs and raises flags to
                    prompt the encoder task to perform actions accordingly.
        @details    This function is called by main and runs every [period]
                    microseconds. In its usual state "S1_WAIT", the task checks
                    the serial port for user inputs and, if any are present,
                    reads and decodes the input. It will set the state to the
                    proper state (ZERO, POSITION, DELTA, DATASTART, or DATAEND)
                    depending on the user input. As it transitions to any state 
                    other than WAIT, the task raises a shared variable flag
                    (zFlag, pFlag, dFlag, gFlag, or sFlag) to indicate to the
                    encoder task that some action must be completed. Once the
                    action has been completed, the encoder task will lower the
                    flag, prompting the user task to return to the "WAIT" state.
                    The task also checks if the shared variable "timeout" has
                    been set to True, in which case the state will be set to
                    DATAEND to end data collection and print the data to
                    PuTTy.
        @param      serport This parameter is the serial port from which the
                    user task will read user input characters.
        @param      zFlag This is the shared variable which, when set to True,
                    indicates to the Encoder task that the encoder should be
                    zeroed.
        @param      dataFlag This shared boolean variable, when False, indicates
                    to the user task that data is not ready to be printed.
        @param      pos This shared variable is used to pass the encoder
                    position from the Encoder task to the User task.
        @param      delta This shared variable is used to pass the encoder
                    delta from the Encoder task to the User task.
        @param      velocity This shared variable is used to pass the encoder
                    velocity from the encoder task to the user task.
        @param      dutycycle1 This shared variable is used to pass the duty
                    cycle for motor 1 from the user task to the motor task.
        @param      dutycycle2 This shared variable is used to pass the duty
                    cycle for motor 2 from the user task to the motor task.
        @param      Driver This driver object is used by the user task to reset
                    motor driver faults
        @param      datarecord This shared boolean variable is set to True to
                    indicate to the data collection task that data recording
                    should be ongoing. To halt data collection, the user task 
                    sets this variable to False.
        @param      timeData This shared variable is used to pass an array of
                    time data from the Encoder task to the User task.
        @param      positionData This shared variable is used to pass an array 
                    of position data from the Encoder task to the User task.
        @param      deltaData This shared variable is used to pass an array of
                    delta data from the Encoder task to the User task.
        @param      timeout This shared variable is used to indicate to the
                    User task that data collection has timed out.
        @param      finalindex This shared variable indicates the index of the
                    last data point collected before data collection was ended.
        @param      period This parameter, in microseconds, indicates to the
                    User task how often it is supposed to run.
    '''
    S0_INIT = const(0)
    S1_WAIT = const(1)
    S2_ZERO = const(2)
    S3_POSITION = const(3)
    S4_DELTA = const(4)
    S5_VELOCITY = const(5)
    S6_DUTY1 = const(6)
    S7_DUTY2 = const(7)
    S8_CLEARFAULT = const(8)
    S9_DATASTART = const(9)
    S10_TEST = const(10)
    S11_DATAEND = const(11)
    
    state = S0_INIT
    starttime = utime.ticks_us()
    nexttime = starttime
    buffer = ''
    
    sFlag = False
    
    motorstabletime = 1_000_000 # Motor speed stabilization time in us
    
    vpoints = 100 # number of points to take for motor speed
    velocitydata = vpoints*[0] # array for velocity data
    averagevelocities = []
    dutycycles = []
    
    while True:
        currenttime = utime.ticks_us()
        if utime.ticks_diff(nexttime, currenttime) <= 0: # check if it's time to run the user task
            nexttime = utime.ticks_add(nexttime, period)
            
            # This conditional checks if the current state is S0_INIT and prints
            # the menu if it is
            if state == S0_INIT:
                print("Welcome to the program!")
                printmenu()
                state = S1_WAIT
                yield None
            
            # This conditional checks if the current state is S1_WAIT
            elif state == S1_WAIT:
                
                # This conditional checks if there are any characters waiting
                # to be read in the serport and, if so, decodes them and checks
                # if they are one of the user inputs. If they are, the state
                # is set accordingly. If not, the menu is printed again.
                if serport.any():
                    charIn = serport.read(1).decode()
                    if charIn in {'z', 'Z'}:
                        zFlag.write(True)
                        print('Zeroing...')
                        state = S2_ZERO
                    elif charIn in {'p', 'P'}:
                        print('Printing position...')
                        state = S3_POSITION
                    elif charIn in {'d', 'D'}:
                        print('Printing delta...')
                        state = S4_DELTA
                    elif charIn in {'v', 'V'}:
                        print('Printing velocity...')
                        state = S5_VELOCITY
                    elif charIn == 'm':
                        buffer = ''
                        print('Enter Duty Cycle (-100 to 100)')
                        while True:
                            if serport.any():
                                charIn = serport.read(1).decode()
                                if charIn.isdigit():
                                    buffer+=charIn
                                elif charIn == '-':
                                    if buffer == '':
                                        buffer +=charIn
                                elif charIn == '.':
                                    if buffer.find('.') == -1:
                                        buffer += charIn
                                elif charIn == '\x7F':
                                    if buffer != '':
                                        buffer = buffer[:-1]
                                elif charIn in {'\r', '\n'}:
                                    if buffer == '':
                                        buffer = str(dutycycle1.read())
                                    break
                                print(buffer)
                                yield None
                            else:
                                yield None
                        if float(buffer) <= 100 and float(buffer) >= -100:
                            state = S6_DUTY1
                        else:
                            print('invalid duty cycle; must be a number between -100 and 100 inclusive')
                    elif charIn == 'M':
                        buffer = ''
                        print('Enter Duty Cycle (-100 to 100)')
                        while True:
                            if serport.any():
                                charIn = serport.read(1).decode()
                                if charIn.isdigit():
                                    buffer+=charIn
                                elif charIn == '-':
                                    if buffer == '':
                                        buffer +=charIn
                                elif charIn == '.':
                                    if buffer.find('.') == -1:
                                        buffer += charIn
                                elif charIn == '\x7F':
                                    if buffer != '':
                                        buffer = buffer[:-1]
                                elif charIn in {'\r', '\n'}:
                                    if buffer == '':
                                        buffer = str(dutycycle1.read())
                                    break
                                print(buffer)
                                yield None
                            else:
                                yield None
                        if float(buffer) <= 100 and float(buffer) >= -100:
                            state = S7_DUTY2
                        else:
                            print('invalid duty cycle; must be a number between -100 and 100 inclusive')
                    elif charIn in {'c', 'C'}:
                        print('Clearing Motor Driver Fault')
                        state = S8_CLEARFAULT
                    elif charIn in {'g', 'G'}:
                        print('Starting data collection')
                        state = S9_DATASTART
                    elif charIn in {'t', 'T'}:
                        print('Starting Testing Interface')
                        state = S10_TEST
                    elif charIn in {'s', 'S'}:
                        print('Terminating data collection')
                        datarecord.write(False)
                        state = S11_DATAEND
                    else:
                        print(f'You entered {charIn}.')
                        printmenu()
                # This conditional checks for timeout and sets the state to
                # DATAEND if timeout has occured.
                elif timeout.read():
                    timeout.write(False)
                    print('Data collection timeout')
                    state = S11_DATAEND
                yield None
            
            # The following set of conditionals check the current state of the
            # user task and raise the proper flag before resetting the state
            # to WAIT
            elif state == S2_ZERO:
                if not zFlag.read():
                    state = S1_WAIT
                    print ('Zeroed!')
                yield None
            elif state == S3_POSITION:
                state = S1_WAIT
                print (f'Position = {pos.read()}')
                yield None
            elif state == S4_DELTA:
                state = S1_WAIT
                print (f'Delta = {delta.read()}')
                yield None
            elif state == S5_VELOCITY:
                state = S1_WAIT
                print (f'Velocity = {velocity.read()}')
                yield None
            elif state == S6_DUTY1:
                print('Motor 1 duty cycle = ' + buffer)
                dutycycle1.write(float(buffer))
                state = S1_WAIT
                yield None
            elif state == S7_DUTY2:
                print('Motor 2 duty cycle = ' + buffer)
                dutycycle2.write(float(buffer))
                state = S1_WAIT
                yield None
            elif state == S8_CLEARFAULT:
                state = S1_WAIT
                Driver.enable()
                print('Fault Cleared')
                yield None
            elif state == S9_DATASTART:
                state = S1_WAIT
                datarecord.write(True)
                print ('Data collection started')
                yield None
            elif state == S10_TEST:
                averagevelocities = []
                dutycycles = []
                while True: # Testing interface main loop
                    buffer = ''
                    print('Enter Duty Cycle (-100 to 100) or press s or S to exit')
                    
                    while True: # Loop for user to input duty cycle
                        if serport.any():
                            charIn = serport.read(1).decode()
                            if charIn in {'s', 'S'}:
                                print('Exiting...')
                                sFlag = True
                                break
                            if charIn.isdigit(): # Check if digit
                                buffer+=charIn
                            elif charIn == '-': # Check if '-'
                                if buffer == '':
                                    buffer +=charIn
                            elif charIn == '.': # Check if '.'
                                if buffer.find('.') == -1:
                                    buffer += charIn
                            elif charIn == '\x7F': # Check if backspace
                                if buffer != '':
                                    buffer = buffer[:-1]
                            elif charIn in {'\r', '\n'}: # Check if enter
                                if buffer == '':
                                    buffer = str(dutycycle1.read())
                                break
                            print(buffer)
                            yield None
                        else:
                            yield None
                    if sFlag == True: # Check if user entered character 's'
                        break
                    if float(buffer) <= 100 and float(buffer) >= -100: # check if duty cycle is valid
                        dutycycle1.write(float(buffer))
                        motortime = utime.ticks_us()
                        while True: # loop for motor speed stabilization
                            currenttime = utime.ticks_us()
                            if utime.ticks_diff(currenttime, motortime) >= motorstabletime:
                                break
                            yield None
                            nexttime = utime.ticks_us()
                            velocityindex = 0
                            
                        print('Motor speed stabilized')
                        while True: # for loop to gather velocity data
                            currenttime = utime.ticks_us()
                            if velocityindex >= vpoints:
                                break
                            if utime.ticks_diff(nexttime, currenttime) <= 0:
                                nexttime = utime.ticks_add(nexttime, period)    
                                velocitydata[velocityindex] = velocity.read()
                                velocityindex += 1
                            yield None
                            
                        # add velocity and duty cycle data to lists
                        velocitypoint = sum(velocitydata)/len(velocitydata)
                        dutycyclepoint = dutycycle1.read()
                        averagevelocities.append(velocitypoint)
                        dutycycles.append(dutycyclepoint)
                        yield None
                        
                    else: # if DC is invalid, notify user and restart outer loop
                        print('invalid duty cycle; must be a number between -100 and 100 inclusive')
                        yield None
                for i in range(len(dutycycles)):
                    print(f'{dutycycles[i]}, {averagevelocities[i]}')
                    yield None
                sFlag = False
                dutycycles = []
                averagevelocities = []
                gc.collect()
                state = S1_WAIT
                print('Testing Complete.')
            elif state == S11_DATAEND:
                state = S1_WAIT
                while not dataFlag.read():
                    yield None
                print ('Data Collection terminated')
                for index in range(finalindex.read()):
                    print(f'{timeData[index]}, {positionData[index]}, {deltaData[index]}')
                    yield None
                finalindex.write(0)
                gc.collect()
                yield None
        else:
            yield None
    
    
def printmenu():
    '''!@brief      This task prints a menu to PuTTy informing the user of the
                    commands they may use.
    '''
    print('+-----------------------------------------------------------------------+')
    print('|--Press z or Z to zero encoder-----------------------------------------|')
    print('|--Press p or P to print encoder position-------------------------------|')
    print('|--Press d or D to print encoder delta----------------------------------|')
    print('|--Press v or V to print encoder velocity-------------------------------|')
    print('|--Press m to enter duty cycle for motor 1------------------------------|')
    print('|--Press M to enter duty cycle for motor 2------------------------------|')
    print('|--Press c or C to clear a motor driver fault---------------------------|')
    print('|--Press g or G to collect 30 seconds of encoder data and print as csv--|')
    print('|--Press t or T to start testing interface------------------------------|')
    print('|--Press s or S to end data collection or testing interface-------------|')
    print('+-----------------------------------------------------------------------+')