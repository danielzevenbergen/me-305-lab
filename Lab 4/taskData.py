'''!@file       taskData.py
    @brief      This file is the task responsible for collection of encoder
                position, delta, and speed data
    @details    This task file has two states: S0_DATAOFF and S1_DATAON. The
                DATAOFF state indicates that data collection is not ongoing,
                and the DATAON state indicates that data collection is ongoing.
                When the shared variable datarecord is set to True by the user
                task, the data task changes its state to DATAON and starts
                recording data. As long as datarecord is true, data is
                collected in the local arrays timearray, positionarray,
                and deltaarray. When data recording begins, the local arrays
                are preallocated to the desired size, based on the value of
                the shared variable datapoints. Data collection ends either
                when the local arrays fill up (timeout) or when the datarecord
                boolean is set to False by the user task.
    @image      html "Lab 3 Data Collection Task FSM.png" "Lab 4 Data Colelction Task Finite State Machine" width=960
    @author     Daniel Zevenbergen
    @date 02/13/2022
'''

import utime
from micropython import const
import gc

def Dtask(datarecord, dataFlag, datapoints, timeData, positionData, deltaData, pos, delta, timeout, finalindex, period):
    '''!@brief      This generator function runs once every [period]
                    microseconds and is responsible for adding the current
                    time, encoder position, and encoder delta to the data arrays
                    timearray, positionarray, and deltaarray
        @param      datarecord A shared boolean variable activated by the user
                    task which, when True, indicates that data collection should
                    be ongoing.
        @param      dataFlag This shared boolean variable, when False, indicates
                    to the user task that data is not ready to be printed.
        @param      datapoints An integer representing the maximum number of
                    data points that may be collected before data collection is
                    automatically halted.
        @param      timeData A shared array variable for time values
        @param      positionData A shared array variable for position values
        @param      deltaData A shared array variable for delta values
        @param      pos A shared variable containing the position of the
                    encoder
        @param      delta A shared variable containing the most recent delta of
                    the encoder
        @param      timeout A shared variable which indicates to the user task 
                    that data collection has timed out.
        @param      finalindex A shared variable which indicates to the user task
                    the final index of data collected.
        @param      period The period at which the data task should run, in
                    microseconds.
    '''
    S0_DATAOFF = const(0)
    S1_DATAON = const(1)
    
    state = S0_DATAOFF
    
    starttime = utime.ticks_us()
    datastarttime = starttime
    nexttime = starttime
    index = 0
    
    
    while True:
        currenttime = utime.ticks_us()
        if utime.ticks_diff(nexttime, currenttime) <= 0:
            nexttime = utime.ticks_add(nexttime, period)
            
            if state == S0_DATAOFF:
                if datarecord.read():
                    index = 0
                    gc.collect()
                    datastarttime = utime.ticks_us()
                    dataFlag.write(False)
                    state = S1_DATAON
                yield None
            elif state == S1_DATAON:
                if index < datapoints:
                    timeData[index] = utime.ticks_diff(utime.ticks_us(), datastarttime)
                    positionData[index] = pos.read()
                    deltaData[index] = delta.read()
                    index += 1
                else:
                    datarecord.write(False)
                    timeout.write(True)
                if not datarecord.read():
                    finalindex.write(index-1)
                    state = S0_DATAOFF
                    dataFlag.write(True)
                yield None
        else:
            yield None
                
            
            
            
            
            
            
            
            
            
            