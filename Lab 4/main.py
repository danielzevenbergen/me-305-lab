'''!@file       main.py
    @brief      This file is the main file for Lab 4. It creates a simple
                user interface, encoder object, motor driver object, and two
                motor objects to allow the user to interact with the motors and
                encoder via a simple UI in PuTTy. It also creates a closed loop
                controller object and accompanying user interface tools to let the
                user perform closed-loop control and step responses on the motor.
    @details    This file creates an encoder object using the encoder pins and
                timer on the STM32 Nucleo board, a motor driver object using the
                timer connected to the driver, two motor objects representing
                the two motors, and a closedloop object to perform closed loop
                control on the motor. This file continually runs the
                user interface task (responsible for reading and managing user
                inputs) the encoder task (responsible for updating and
                otherwise interacting with the encoder object), the motor tasks
                (responsible for updating the duty cycle of the two motors),
                the data collection task (responsible for recording data when
                prompted to do so by the user), and the controller task (responsible
                for controlling the motor speed). This file
                initializes all the variables required for the aforementioned
                tasks to run properly.
    @image      html Encoder_Position_vs_Time.png "Plot of encoder position data versus time" width=1127 height=750
    @image      html "Motor Duty Velocity vs Duty Cycle.png" "Plot of motor 1 velocity versus duty cycle" width=1150
    @image      html "Lab 4 Task Diagram.png" "Task Diagram for Lab 4" width=960
    @author     Daniel Zevenbergen
    @date       02/02/2022
'''
import shares
import pyb
import encoder
import DRV8847
import closedloop
import taskEncoder
import taskUser
import taskMotor
import taskData
import taskController
import array
import gc

if __name__ == '__main__':
    pinENCA = pyb.Pin(pyb.Pin.board.PB6)
    pinENCB = pyb.Pin(pyb.Pin.board.PB7)
    timer = pyb.Timer(4, prescaler=0, period=65535)
    timerch1 = timer.channel(1, pyb.Timer.ENC_AB, pin=pinENCA)
    timerch2 = timer.channel(2, pyb.Timer.ENC_AB, pin=pinENCB)
    myEncoder = encoder.Encoder(pinENCA, pinENCB, timer, 0)
    
    Pin_nSleep = pyb.Pin(pyb.Pin.board.PA15, mode=pyb.Pin.OUT)
    Pin_nFault = pyb.Pin(pyb.Pin.board.PB2, mode=pyb.Pin.OPEN_DRAIN)
    Pin_IN1 = pyb.Pin(pyb.Pin.board.PB4, mode=pyb.Pin.OUT)
    Pin_IN2 = pyb.Pin(pyb.Pin.board.PB5, mode=pyb.Pin.OUT)
    Pin_IN3 = pyb.Pin(pyb.Pin.board.PB0, mode=pyb.Pin.OUT)
    Pin_IN4 = pyb.Pin(pyb.Pin.board.PB1, mode=pyb.Pin.OUT)
    myDriver = DRV8847.DRV8847(3, Pin_nSleep, Pin_nFault, Pin_IN1, Pin_IN2, Pin_IN3, Pin_IN4)
    
    myMotor1 = myDriver.motor(1)
    myMotor2 = myDriver.motor(2)
    
    datapoints = 901 # number of data points to be recorded by data task
    datarecord = shares.Share(False) # indicates whether data recording is in progress
    
    zFlag = shares.Share(False)
    dataFlag = shares.Share(True)
    pos = shares.Share(0)
    delta = shares.Share(0)
    velocity=shares.Share(0)
    timeData = array.array('l', datapoints*[0])
    gc.collect()
    positionData = array.array('l', timeData)
    gc.collect()
    deltaData = array.array('l', timeData)
    timeout = shares.Share(False)
    finalindex = shares.Share(0)
    dutycycle1 = shares.Share(0)
    dutycycle2 = shares.Share(0)
    
    K_p = shares.Share(0)
    K_i = shares.Share(0)
    K_d = shares.Share(0)
    reference = shares.Share(0)
    closedloopON = shares.Share(False)
    saturationlow = -100
    saturationhigh = 100
    
    driverfault = shares.Share(False)
    
    controller = closedloop.ClosedLoop(reference.read(), velocity.read(), K_p.read(), K_i.read(), K_d.read(), saturationlow, saturationhigh)
    
    Userfreq = 100 # User task frequency in Hz
    EncoderFreq = 1000 # Encoder task frequency in Hz
    MotorFreq = 1000 # Motor task frequency in Hz
    DataFreq = 30 # Data task frequency in Hz
    ControllerFreq = 1000 # Controller task frequency in Hz
    Userperiod = 1_000_000//Userfreq # User task period in us
    Encoderperiod = 1_000_000//EncoderFreq # Encoder task period in us
    Motorperiod = 1_000_000//MotorFreq # Motor task period in us
    Dataperiod = 1_000_000//DataFreq # Data task period in us
    Controllerperiod = 1_000_000//ControllerFreq # Controller task period in us
    serport = pyb.USB_VCP()
    
    task_Encoder = taskEncoder.Etask(myEncoder, zFlag, pos, delta, velocity, Encoderperiod)
    task_User = taskUser.Utask(serport, zFlag, dataFlag, pos, delta, velocity, dutycycle1, dutycycle2, myDriver, reference, K_p, K_i, K_d, closedloopON, datarecord, timeData, positionData, deltaData, timeout, finalindex, driverfault, Userperiod)
    task_Motor1 = taskMotor.Mtask(myMotor1, myDriver, driverfault, dutycycle1, Motorperiod)
    task_Motor2 = taskMotor.Mtask(myMotor2, myDriver, driverfault, dutycycle2, Motorperiod)
    task_Data = taskData.Dtask(datarecord, dataFlag, datapoints, timeData, positionData, deltaData, pos, delta, timeout, finalindex, Dataperiod)
    task_Controller = taskController.Ctask(controller, K_p, K_i, K_d, reference, velocity, dutycycle1, closedloopON, Controllerperiod)
    tasklist = [task_Encoder, task_User, task_Motor1, task_Motor2, task_Data, task_Controller]
    
    while True:
        try:
            for task in tasklist:
                next(task) 
        except KeyboardInterrupt:
            break