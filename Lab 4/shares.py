'''!@file       shares.py
    @brief      Task sharing library implementing both shares and queues.
    @details    Implements a very simple interface for sharing data between
                multiple tasks.
'''

import array

class Share:
    '''!@brief      A standard shared variable.
        @details    Values can be accessed with read() or changed with write()
    '''
    def __init__(self, initial_value=None):
        '''!@brief      Constructs a shared variable
            @param      initial_value An optional initial value for the 
                                      shared variable.
        '''
        self._buffer = initial_value
    
    def write(self, item):
        '''!@brief      Updates the value of the shared variable
            @param item The new value for the shared variable
        '''
        self._buffer = item
        
    def read(self):
        '''!@brief      Access the value of the shared variable
            @return    The value of the shared variable
        '''
        return self._buffer
    
class SharedArray:
    '''!@brief      A shared array variable.
        @details    single values can be accessed with readsingle() and
                    writesingle(), or the entire array can be accessed with
                    read() or write()
    '''
    def __init__(self, datatype, length=0):
        '''!@brief      Constructs a shared array
            @details    Constructs a shared array with data type [datatype]
                        and length [length]
        '''
        self._buffer = array.array(datatype, length*[0])
        self.dataype = datatype
        self.length = length
        
    def write(self, arr):
        '''!@brief      Update the value of the shared array to [arr] if [arr]
                        is an array
            @param      arr This is array meant to replace the existing value
                        of the object
        '''
        if isinstance(arr, array.array):
            self._buffer = arr
            
    def read(self):
        '''!@brief      return the value of the array
            @return     returns the value of the array
        '''
        return self._buffer
    
    def writesingle(self, item, index):
        '''!@brief      Updates the value at index [index] with [item] if [index]
                        falls within the length of the array and if [item]
                        has the correct data type
            @param      item This is the item to be placed at the given index
            @param      index The index at which the item should be placed
        '''
        if index<self.length:
            self._buffer[index] = item
        
    def readsingle(self, index):
        '''!@brief      Returns the value of the item at index [index]
            @param      index The index of the value to be returned
        '''
        return self._buffer[index]
        
class Queue:
    '''!@brief      A queue of shared data.
        @details    Values can be accessed with placed into queue with put() or
                    removed from the queue with get(). Check if there are
                    items in the queue with num_in() before using get().
    '''
    def __init__(self):
        '''!@brief              Constructs an empty queue of shared values
        '''
        self._buffer = []
    
    def put(self, item):
        '''!@brief      Adds an item to the end of the queue.
            @param item The new item to append to the queue.
        '''
        self._buffer.append(item)
        
    def get(self):
        '''!@brief      Remove the first item from the front of the queue
            @return     The value of the item removed
        '''
        return self._buffer.pop(0)
    
    def num_in(self):
        '''!@brief      Find the number of items in the queue. Call before get().
            @return     The number of items in the queue
        '''
        return len(self._buffer)