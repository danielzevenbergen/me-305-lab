'''!@file       taskController.py
    @brief      This task manages the closedloop object controlling the motor
    @details    This task has two states, INACTIVE and ACTIVE. When inactive,
                it only checks if closedloopON has been set to True by the user
                task and, if not, yields None. When active, it updates the gains
                and reference value of the controller object, then runs the controller.
    @image      html "Lab 4 Controller Task FSM.png" "Lab 4 controller task finite state machine" width=960
    @author     Daniel Zevenbergen
    @date       02/07/2022
'''

import utime
from micropython import const

def Ctask(controller, K_p, K_i, K_d, reference, velocity, dutycycle, closedloopON, period):
    '''!@brief      This generator function runs at period [period] and manages
                    the ClosedLoop object [controller]
        @details
        @param      controller An instance of the closedloop.ClosedLoop class to
                    be managed by this task.
        @param      K_p A shared variable used to pass the proportional gain from
                    the user task to this task.
        @param      K_i A shared variable used to pass the integral gain from
                    the user task to this task.
        @param      K_d A shared variable used to pass the derivative gain from
                    the user task to this task.
        @param      reference A shared variable used to pass the reference value
                    (setpoint) from the user task to this task.
        @param      velocity A shared variable representing the velocity of the motor.
        @param      dutycycle A shared variable representing the duty cycle of the motor.
        @param      closedloopON A shared boolean variable indicating whether
                    closed loop control should be on.
        @param      period The period at which this task should run, in microseconds.
    '''
    
    S0_INACTIVE = const(0)
    S1_ACTIVE = const(1)
    
    state = S0_INACTIVE
    
    starttime = utime.ticks_us()
    nexttime = starttime
    while True:
        currenttime = utime.ticks_us()
        if utime.ticks_diff(nexttime, currenttime) <= 0:
            nexttime = utime.ticks_add(currenttime, period)
            
            if state == S0_INACTIVE:
                if closedloopON.read():
                    controller.activate()
                    state = S1_ACTIVE
            
            elif state == S1_ACTIVE:
                controller.set_Kp(K_p.read())
                controller.set_Ki(K_i.read())
                controller.set_Kd(K_d.read())
                controller.set_reference(reference.read())
                dutycycle.write(controller.run(velocity.read()))
                if not closedloopON.read():
                    controller.deactivate()
                    state = S0_INACTIVE
                    
            yield None
        else:
            yield None
            
            
            
            
            
            
            