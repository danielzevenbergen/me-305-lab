'''!@file       touchpanel.py
    @brief      This file creates a touch panel object which can be used to read
                values from a resistive touch panel.
    @author     Daniel Zevenbergen
    @date       03/10/2022
'''

from pyb import ADC, Pin, Timer
from ulab import numpy
import utime
import micropython
import array

class TouchPanel:
    '''!@brief      This class is used to create touch panel objects that interface
                    with a resistive touch panel.
        @details    This class takes as inputs four strings representing the
                    pins connected to the touch panel. It uses these strings to
                    initialize the pins to the correct mode whenever a value
                    must be read from the touch panel. This class provides
                    methods that allow for reading of any of the three components
                    (x, y, z) from the touch panel, and a method to read all three
                    in sequence and return their values as a tuple.
    '''
    
    def __init__ (self, y_m, x_m, y_p, x_p, x, y):
        '''!@brief      This constructor method creates a touchpanel object
            @details    This constructor accpets as arguments four strings, representing
                        the four pins connected to the touch panel, and two integers
                        representing the x- and y-dimensions of the panel in mm.
                        The constructor saves these as attributes and creates a timer
                        that will be used for ADC oversampling, as well as a buffer used
                        to store the values sampled from the ADC.
            @param      y_m A string representing the board pin identifier for the y_m pin of the touch panel
            @param      x_m A string representing the board pin identifier for the x_m pin of the touch panel
            @param      y_p A string representing the board pin identifier for the y_p pin of the touch panel
            @param      x_p A string representing the board pin identifier for the x_p pin of the touch panel
            @param      x The x dimension of the touch panel in mm
            @param      y The y dimension of the touch panel in mm
        '''
        ## a string representing the pin connected to the y_m pin of the touch panel
        self.y_m = y_m
        ## a string representing the pin connected to the x_m pin of the touch panel
        self.x_m = x_m
        ## a string representing the pin connected to the y_p pin of the touch panel
        self.y_p = y_p
        ## a string representing the pin connected to the x_p pin of the touch panel
        self.x_p = x_p
        ## An integer representing the x-dimension of the panel in mm
        self.x = x
        ## An integer representing the y-dimension of the panel in mm
        self.y = y
        ## A matrix containing panel calibration coefficients
        self.beta = numpy.array([[0, 0], [0, 0], [0, 0]])
        ## A timer used for ADC oversampling
        self.ADCtim = Timer(6, freq=10_000_000)
        ## An int representing the number of data points to sample from the ADC
        self.points = 20
        ## An array used to store values from ADC sampling
        self.buf = array.array('i', self.points*[0])
        
    @micropython.native
    def xscan(self):
        '''!@brief      This method returns the corrected x position of the contact point
            @details    This method initializes the four touch panel pins in the proper
                        modes to read the x-position of the contact point on the touch panel.
                        It then initializes an ADC to read the output of the y_m pin and reads
                        several values into self.buf. It then returns the average of these values.
            @return     Returns the x position in mm from the center of the panel
        '''
        y_m = Pin(self.y_m, mode=Pin.ANALOG)
        x_m = Pin(self.x_m, mode=Pin.OUT, value=0)
        y_p = Pin(self.y_p, mode=Pin.OPEN_DRAIN)
        x_p = Pin(self.x_p, mode=Pin.OUT, value=1)
        adc = ADC(y_m)
        buf = self.buf
        adc.read_timed(buf, self.ADCtim)
        return sum(buf)/self.points*self.x/4096-self.x/2
    
    @micropython.native
    def yscan(self):
        '''!@brief      This method returns the corrected y position of the contact point
            @details    This method initializes the four touch panel pins in the proper
                        modes to read the y-position of the contact point on the touch panel.
                        It then initializes an ADC to read the output of the x_m pin and reads
                        several values into self.buf. It then returns the average of these values.
            @return     Returns the y position in mm from the center of the panel
        '''
        y_m = Pin(self.y_m, mode=Pin.OUT, value=0)
        x_m = Pin(self.x_m, mode=Pin.ANALOG)
        y_p = Pin(self.y_p, mode=Pin.OUT, value=1)
        x_p = Pin(self.x_p, mode=Pin.OPEN_DRAIN)
        adc = ADC(x_m)
        buf = self.buf
        adc.read_timed(buf, self.ADCtim)
        return sum(buf)/self.points*self.y/4096-self.y/2
    
    @micropython.native
    def zscan(self):
        '''!@brief      This method returns True if there is contact on the touch panel and False otherwise
            @details    This method initializes the four touch panel pins in the proper
                        modes to read the z-value of the touch panel.
                        It then initializes an ADC to read the output of the y_m pin and reads
                        several values into self.buf. It then averages the values
                        and checks if the average is below a certain threshold. If
                        it is above, the method returns True for contact. Otherwise
                        it returns False.
            @return     True if there is contact with the panel, False otherwise.
        '''
        y_m = Pin(self.y_m, mode=Pin.ANALOG)
        x_m = Pin(self.x_m, mode=Pin.OUT, value=0)
        y_p = Pin(self.y_p, mode=Pin.OUT, value=1)
        x_p = Pin(self.x_p, mode=Pin.OPEN_DRAIN)
        adc = ADC(y_m)
        buf = self.buf
        adc.read_timed(buf, self.ADCtim)
        y_mread = adc.read()
        if sum(buf)/self.points<4050:
            return True
        else:
            return False
        
    @micropython.native
    def scan3(self):
        '''!@brief      This method performs an x-, y-, and z-scan
            @details    This method initializes the four touch panel pins in the proper
                        modes to read the x-position of the contact point on the touch panel.
                        It then initializes an ADC to read the output of the y_m pin and reads
                        several values into self.buf. It then stores the average of these values
                        in xread. Two pins are then reinitialized to read the z-value
                        of the panel. Several values are read and the average is compared
                        to a threshold to determine whether there is contact with the
                        panel. zread is set to either True or False depending on the result.
                        Two pins are reinitialized to read the y-position of the contact point.
                        The adc is reinitialized to read from the x_m pin and reads several values into buf.
                        These values are averaged and stored in yread. The method returns a tuple
                        consisting of xread, yread, and zread.
            @return     Returns a tuple containing the x-reading, y-reading, and z-reading
        '''
        y_m = Pin(self.y_m, mode=Pin.ANALOG)
        x_m = Pin(self.x_m, mode=Pin.OUT, value=0)
        y_p = Pin(self.y_p, mode=Pin.OPEN_DRAIN)
        x_p = Pin(self.x_p, mode=Pin.OUT, value=1)
        adc = ADC(y_m)
        buf = self.buf
        ADCtim = self.ADCtim
        adc.read_timed(buf, ADCtim)
        points = self.points
        xread = sum(buf)/points
        y_p = Pin(self.y_p, mode=Pin.OUT, value=1)
        x_p = Pin(self.x_p, mode=Pin.OPEN_DRAIN)
        adc.read_timed(buf, ADCtim)
        if sum(buf)/points<4050:
            zread=True
        else:
            zread=False
        x_m = Pin(self.x_m, mode=Pin.ANALOG)
        y_m = Pin(self.y_m, mode=Pin.OUT, value=0)
        adc = ADC(x_m)
        adc.read_timed(buf, ADCtim)
        yread = sum(buf)/points
        x = self.x
        y = self.y
        return xread*x/4096-x/2, yread*y/4096-y/2, zread
    
    
if __name__ == "__main__":
    y_m = 'PA0'
    x_m = 'PA1'
    y_p = 'PA6'
    x_p = 'PA7'
    x = 176
    y = 100
    panel = TouchPanel(y_m, x_m, y_p, x_p, x, y)
    starttime = utime.ticks_us()
    ind = 0
    while ind < 1000:
        xpos = panel.xscan()
        ind += 1
    finaltime = utime.ticks_us()
    elapsedtime = utime.ticks_diff(finaltime, starttime)
    print(f'Time for 1000 x-scans = {elapsedtime} microseconds')
    print(f'Time for 1 x-scan = {elapsedtime/1000} microseconds')
    starttime = utime.ticks_us()
    ind = 0
    while ind < 1000:
        ypos = panel.yscan()
        ind += 1
    finaltime = utime.ticks_us()
    elapsedtime = utime.ticks_diff(finaltime, starttime)
    print(f'Time for 1000 y-scans = {elapsedtime} microseconds')
    print(f'Time for 1 y-scan = {elapsedtime/1000} microseconds')
    starttime = utime.ticks_us()
    ind = 0
    while ind < 1000:
        zpos = panel.zscan()
        ind += 1
    finaltime = utime.ticks_us()
    elapsedtime = utime.ticks_diff(finaltime, starttime)
    print(f'Time for 1000 z-scans = {elapsedtime} microseconds')
    print(f'Time for 1 z-scan = {elapsedtime/1000} microseconds')
    starttime = utime.ticks_us()
    ind = 0
    while ind < 1000:
        scan3 = panel.scan3()
        ind += 1
    finaltime = utime.ticks_us()
    elapsedtime = utime.ticks_diff(finaltime, starttime)
    print(f'Time for 1000 3-scans = {elapsedtime} microseconds')
    print(f'Time for 1 3-scan = {elapsedtime/1000} microseconds')
        
        
        
        
        
        
        
        
        
        
        
        