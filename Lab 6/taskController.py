'''!@file       taskController.py
    @brief      This task manages the closedloop object controlling the motor
    @details    This file contains a generator function that implements a finite
                state machine to manage a closedloop object to perform closed loop
                control on an abritrary output with an abritrary input.
    @image      html ControllerTask_State_Transition_Diagram.png "Controller Task Finite State Machine" width=960
    @author     Daniel Zevenbergen
    @date       02/07/2022
'''

import utime
from micropython import const

def Ctask(controllerOUTER, controllerINNER, K_pOUT, K_iOUT, K_dOUT, K_pIN, K_iIN, K_dIN, outerinvert, reference, ball_position, ball_velocity, ball_z, plat_position, plat_velocity, dutycycle, closedloopON, period):
    '''!@brief      This generator function runs at period [period] and manages
                    the ClosedLoop object [controller]
        @param      controllerOUTER An instance of the closedloop.ClosedLoop class to
                    be managed by this task. Represents the outer control loop
                    in a cascaded control system.
        @param      controllerINNER An instance of the closedloop.ClosedLoop class to
                    be managed by this task. Represents the inner control loop
                    in a cascaded control system.
        @param      K_pOUT A shared variable used to pass the proportional gain from
                    the user task to this task. Used for the outer control loop.
        @param      K_iOUT A shared variable used to pass the integral gain from
                    the user task to this task. Used for the outer control loop.
        @param      K_dOUT A shared variable used to pass the derivative gain from
                    the user task to this task. Used for the outer control loop.
        @param      K_pIN A shared variable used to pass the proportional gain from
                    the user task to this task. Used for the inner control loop.
        @param      K_iIN A shared variable used to pass the integral gain from
                    the user task to this task. Used for the inner control loop.
        @param      K_dIN A shared variable used to pass the derivative gain from
                    the user task to this task. Used for the inner control loop.
        @param      outerinvert A variable, either 1 or -1, indicating whether the
                    outer loop output should be inverted before being input
                    to the inner loop.
        @param      reference A shared variable used to pass the reference value
                    (setpoint) for the outer loop from the user task to this task.
        @param      ball_position A shared variable representing the position of the ball on the panel in one axis.
        @param      ball_velocity A shared variable representing the velocity of the ball on the panel in one axis.
        @param      plat_position A shared variable representing the angular position of the platform in one axis.
        @param      plat_velocity A shared variable representing the angular velocity of the platform in one axis.
        @param      dutycycle A shared variable to be updated as the output of the closed-loop controller.
        @param      closedloopON A shared boolean variable indicating whether
                    closed loop control should be on.
        @param      period The period at which this task should run, in microseconds.
    '''
    
    S0_INACTIVE = const(0)
    S1_ACTIVE = const(1)
    
    state = S0_INACTIVE
    
    starttime = utime.ticks_us()
    nexttime = starttime
    while True:
        currenttime = utime.ticks_us()
        if utime.ticks_diff(nexttime, currenttime) <= 0:
            nexttime = utime.ticks_add(currenttime, period)
            
            if state == S0_INACTIVE:
                if closedloopON.read():
                    controllerOUTER.activate()
                    controllerINNER.activate()
                    state = S1_ACTIVE
            
            elif state == S1_ACTIVE:
                if ball_z.read():
                    controllerOUTER.set_Kp(K_pOUT.read())
                    controllerOUTER.set_Ki(K_iOUT.read())
                    controllerOUTER.set_Kd(K_dOUT.read())
                    controllerOUTER.set_reference(reference.read())
                    controllerINNER.set_reference(outerinvert*controllerOUTER.run(ball_position.read(), ball_velocity.read()))
                    controllerINNER.set_Kp(K_pIN.read())
                    controllerINNER.set_Ki(K_iIN.read())
                    controllerINNER.set_Kd(K_dIN.read())
                    dutycycle.write(-1*controllerINNER.run(plat_position.read(), plat_velocity.read()))
                else:
                    controllerINNER.set_reference(0)
                    controllerINNER.set_Kp(0.4)
                    controllerINNER.set_Ki(0)
                    controllerINNER.set_Kd(.01)
                    dutycycle.write(-1*controllerINNER.run(plat_position.read(), plat_velocity.read()))
                if not closedloopON.read():
                    controllerOUTER.deactivate()
                    controllerINNER.deactivate()
                    dutycycle.write(0)
                    state = S0_INACTIVE
                    
            yield None
        else:
            yield None
            
            
            
            
            
            
            