'''!@file       motor.py
    @brief      This file creates a motor object which can be used to set the
                duty cycle of a permanent magnet DC motor
    @author     Daniel Zevenbergen
    @date       02/16/2022
'''

from pyb import Pin, Timer

class Motor:
    '''!@brief      This class is used to create motor objects that interface
                    with permanent magnet DC motors.
        @details    Objects of this class are PMDC motors that are to be
                    controlled using pulse width modulation (PWM). Upon creation,
                    the motor object is passed a timer object and two timer channels
                    which must be pre-configured for PWM_Inverted on the input
                    pins of the motor. The set_duty function can be called with
                    a duty cycle argument between -100 and 100 inclusive to
                    update the duty cycle of the motor.
    '''
    
    def __init__ (self, PWM_tim, timch1, timch2):
        '''!@brief      This constructor creates a motor object
            @details    The constructor accepts three arguements: a timer and
                        two corresponding timer channels, both of which should
                        be configured in PWM_INVERTED mode. They should also
                        be attached to the two input pins of the motor.
            @param      PWM_tim A timer object representing the hardware timer
                        connected to the motor driver.
            @param      timch1 A timer channel connected to PWM_tim and the
                        first input pin of the motor.
            @param      timch2 A timer channel connected to PWM_tim and the
                        second input pin of the motor.
        '''
        ## A timer configured for PWM, used for motor PWM
        self.timer = PWM_tim
        ## A timer channel on one of the input pins of the motor. Used for PWM control
        self.timerch1 = timch1
        ## A timer channel on the other of the input pins of the motor. Used for PWM control
        self.timerch2 = timch2
        
    def set_duty(self, duty):
        '''!@brief      Updates the duty cycle of the motor to [duty]. [duty]
                        should be between -100 and 100 inclusive.
            @param      duty The duty cycle that the motor should be set to
        '''
        if duty>=0:
            self.timerch1.pulse_width_percent(0)
            self.timerch2.pulse_width_percent(duty)
        else:
            self.timerch2.pulse_width_percent(0)
            self.timerch1.pulse_width_percent(-1*duty)
            
if __name__ == '__main__':
    PWM_tim = Timer(3, freq=20000)
    nSleep = Pin(Pin.board.PA15, mode=Pin.OUT)
    nSleep.on()
    IN1 = Pin(Pin.board.PB4, mode=Pin.OUT)
    IN2 = Pin(Pin.board.PB5, mode=Pin.OUT)
    IN3 = Pin(Pin.board.PB0, mode=Pin.OUT)
    IN4 = Pin(Pin.board.PB1, mode=Pin.OUT)
    timch1 = PWM_tim.channel(1, Timer.PWM_INVERTED, pin=IN1)
    timch2 = PWM_tim.channel(2, Timer.PWM_INVERTED, pin=IN2)
    timch3 = PWM_tim.channel(3, Timer.PWM_INVERTED, pin=IN3)
    timch4 = PWM_tim.channel(4, Timer.PWM_INVERTED, pin=IN4)
    timch1.pulse_width_percent(0)
    timch2.pulse_width_percent(0)
    timch3.pulse_width_percent(0)
    timch4.pulse_width_percent(0)
    motor_1 = Motor(PWM_tim, timch1, timch2)
    motor_1.set_duty(100.0)
    motor_2 = Motor(PWM_tim, timch3, timch4)
    motor_2.set_duty(0)
    while True:
        try:
            pass
        except KeyboardInterrupt:
            motor_1.set_duty(0)
            motor_2.set_duty(0)
    
    
    
    
    
    
    
    
    
        