'''!@file       main.py
    @brief      This file is the main file for the term project. It creates a simple
                user interface, IMU object, touch panel object, two motor objects,
                and four closed loop controller objects to balance a ball on
                a touch panel while controlling the angle with the two brushed
                DC motors.
    @details    This file creates the required objects and tasks (implemented as
                finite state machines) to balance a ball on a touch panel. A Bosch
                BNO055 IMU is used to measure the orientation of the platter in
                space for closed-loop control. A "BNO055" object is created to interface
                with the IMU and an IMU task is implemented to regularly retrieve
                the orientation of the IMU in the form of euler angles. The IMU task
                also handles calibration of the IMU and calibration file I/O. 
                A four-wire resistive touch panel is used to measure the position
                of the ball on the platter. This is done with a "touchpanel" object that
                reads from the touch panel. A touch panel task is implemented to manage
                the touch panel object. This consists of updating the position regularly
                and handling touch panel calibration. Two motor objects and corresponding
                tasks are also implemented to regularly update the duty cycles of the
                two brushed DC motors. Four closed-loop controller objects and
                two controller tasks are used to implement a cascaded controller
                architecture in both axes. The outer loop takes ball position as
                its input and outputs platform angle, which is used as the reference
                value for the inner loop. The inner loop outputs a duty cycle to
                the motor. A basic, text-based user interface is also implemented
                to allow interaction with the hardware. The user is allowed to
                print ball or platform positions or velocities, zero the IMU,
                select the setpoint and gains for closed-loop control, turn closed-loop control
                on or off, and collect ball and platform position data.
    @author     Daniel Zevenbergen
    @date       03/17/2022
'''
import shares
import pyb
from pyb import Timer
import BNO055
import touchpanel
import motor
import closedloop
import taskIMU
import taskTouchPanel
import taskUser
import taskMotor
import taskData
import taskController
import array
import gc
from pyb import I2C

if __name__ == '__main__':
    pinENCA = pyb.Pin(pyb.Pin.board.PB6)
    pinENCB = pyb.Pin(pyb.Pin.board.PB7)
    timer = pyb.Timer(4, prescaler=0, period=65535)
    timerch1 = timer.channel(1, pyb.Timer.ENC_AB, pin=pinENCA)
    timerch2 = timer.channel(2, pyb.Timer.ENC_AB, pin=pinENCB)
    
    Pin_IN1 = pyb.Pin(pyb.Pin.board.PB4, mode=pyb.Pin.OUT)
    Pin_IN2 = pyb.Pin(pyb.Pin.board.PB5, mode=pyb.Pin.OUT)
    Pin_IN3 = pyb.Pin(pyb.Pin.board.PB0, mode=pyb.Pin.OUT)
    Pin_IN4 = pyb.Pin(pyb.Pin.board.PB1, mode=pyb.Pin.OUT)
    PWM_tim = Timer(3, freq=20000)
    timch1 = PWM_tim.channel(1, Timer.PWM_INVERTED, pin=Pin_IN1)
    timch2 = PWM_tim.channel(2, Timer.PWM_INVERTED, pin=Pin_IN2)
    timch3 = PWM_tim.channel(3, Timer.PWM_INVERTED, pin=Pin_IN3)
    timch4 = PWM_tim.channel(4, Timer.PWM_INVERTED, pin=Pin_IN4)
    
    
    myMotor1 = motor.Motor(PWM_tim, timch1, timch2)
    myMotor2 = motor.Motor(PWM_tim, timch3, timch4)
    datapoints = 601 # number of data points to be recorded by data task
    datarecord = shares.Share(False) # indicates whether data recording is in progress
    
    dataFlag = shares.Share(True)
    pos = shares.Share([0, 0, 0])
    velocity=shares.Share([0, 0, 0])
    rollang = shares.Share(0)
    pitchang = shares.Share(0)
    rollvel = shares.Share(0)
    pitchvel = shares.Share(0)
    ballpos_X = shares.Share(0)
    ballpos_Y = shares.Share(0)
    ballvel_X = shares.Share(0)
    ballvel_Y = shares.Share(0)
    timeData = array.array('l', datapoints*[0])
    gc.collect()
    channel1 = array.array('f', timeData)
    gc.collect()
    channel2 = array.array('f', timeData)
    gc.collect()
    channel3 = array.array('f', timeData)
    gc.collect()
    channel4 = array.array('f', timeData)
    timeout = shares.Share(False)
    finalindex = shares.Share(0)
    dutycycle1 = shares.Share(0)
    dutycycle2 = shares.Share(0)
    
    calibstat = shares.Share(0)
    IMU_calibrated = shares.Share(False)
    
    Panel_calibrated = shares.Share(False)
    
    K_pOUT = shares.Share(0)
    K_iOUT = shares.Share(0)
    K_dOUT = shares.Share(0)
    K_pIN = shares.Share(0)
    K_iIN = shares.Share(0)
    K_dIN = shares.Share(0)
    referenceX = shares.Share(0)
    referenceY = shares.Share(0)
    closedloopON = shares.Share(False)
    saturationlowOUT = -100
    saturationhighOUT = 100
    saturationlowIN = -60
    saturationhighIN = 60
    
    y_m = 'PA0'
    x_m = 'PA1'
    y_p = 'PA6'
    x_p = 'PA7'
    x = 176
    y = 100
    
    panel = touchpanel.TouchPanel(y_m, x_m, y_p, x_p, x, y)
    
    ballpos_X = shares.Share(0)
    ballpos_Y = shares.Share(0)
    ballvel_X = shares.Share(0)
    ballvel_Y = shares.Share(0)
    ball_z = shares.Share(False)
    
    controllerXOUT = closedloop.ClosedLoop(referenceX.read(), ballpos_X.read(), ballvel_X.read(), K_pOUT.read(), K_iOUT.read(), K_dOUT.read(), saturationlowOUT, saturationhighOUT)
    controllerYOUT = closedloop.ClosedLoop(referenceY.read(), ballpos_Y.read(), ballvel_Y.read(), K_pOUT.read(), K_iOUT.read(), K_dOUT.read(), saturationlowOUT, saturationhighOUT)
    
    controllerXIN = closedloop.ClosedLoop(0, 0, 0, K_pIN.read(), K_iIN.read(), K_dIN.read(), saturationlowIN, saturationhighIN)
    controllerYIN = closedloop.ClosedLoop(0, 0, 0, K_pIN.read(), K_iIN.read(), K_dIN.read(), saturationlowIN, saturationhighIN)             
    
    bus = I2C(1)
    bus.init(I2C.CONTROLLER)
    BNO055 = BNO055.BNO055(bus)
    
    Userfreq = 100 # User task frequency in Hz
    IMUFreq = 200 # Encoder task frequency in Hz
    TouchPanelFreq = 200 # Touch panel task frequency in Hz
    MotorFreq = 200 # Motor task frequency in Hz
    DataFreq = 30 # Data task frequency in Hz
    ControllerFreq = 200 # Controller task frequency in Hz
    Userperiod = 1_000_000//Userfreq # User task period in us
    IMUperiod = 1_000_000//IMUFreq # Encoder task period in us
    TouchPanelperiod = 1_000_000//TouchPanelFreq # Touch Panel task period in us
    Motorperiod = 1_000_000//MotorFreq # Motor task period in us
    Dataperiod = 1_000_000//DataFreq # Data task period in us
    Controllerperiod = 1_000_000//ControllerFreq # Controller task period in us
    serport = pyb.USB_VCP()
    
    task_IMU = taskIMU.IMUtask(BNO055, pos, velocity, rollang, rollvel, pitchang, pitchvel, calibstat, IMU_calibrated, IMUperiod)
    task_Touchpanel = taskTouchPanel.touchpaneltask(panel, ballpos_X, ballpos_Y, ballvel_X, ballvel_Y, ball_z, Panel_calibrated, TouchPanelperiod)
    task_User = taskUser.Utask(serport, dataFlag, pos, velocity, ballpos_X, ballpos_Y, ballvel_X, ballvel_Y, ball_z, dutycycle1, dutycycle2, referenceX, K_pOUT, K_iOUT, K_dOUT, K_pIN, K_iIN, K_dIN, closedloopON, datarecord, timeData, channel1, channel2, channel3, channel4, timeout, finalindex, calibstat, IMU_calibrated, Panel_calibrated, BNO055, Userperiod)
    task_Motor1 = taskMotor.Mtask(myMotor1, dutycycle1, Motorperiod)
    task_Motor2 = taskMotor.Mtask(myMotor2, dutycycle2, Motorperiod)
    task_Data = taskData.Dtask(datarecord, dataFlag, datapoints, timeData, channel1, channel2, channel3, channel4, rollang, pitchang, rollvel, pitchvel, ballpos_X, ballpos_Y, ballvel_X, ballvel_Y, dutycycle1, dutycycle2, timeout, finalindex, Dataperiod)
    task_ControllerX = taskController.Ctask(controllerXOUT, controllerXIN, K_pOUT, K_iOUT, K_dOUT, K_pIN, K_iIN, K_dIN, 1, referenceX, ballpos_X, ballvel_X, ball_z, rollang, rollvel, dutycycle1, closedloopON, Controllerperiod)
    task_ControllerY = taskController.Ctask(controllerYOUT, controllerYIN, K_pOUT, K_iOUT, K_dOUT, K_pIN, K_iIN, K_dIN, -1, referenceY, ballpos_Y, ballvel_Y, ball_z, pitchang, pitchvel, dutycycle2, closedloopON, Controllerperiod)
    tasklist = [task_IMU, task_Touchpanel, task_User, task_Motor1, task_Motor2, task_Data, task_ControllerX, task_ControllerY]
    
    while True:
        try:
            for task in tasklist:
                next(task) 
        except KeyboardInterrupt:
            break