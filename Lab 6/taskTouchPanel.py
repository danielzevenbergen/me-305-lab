'''!@file       taskTouchPanel.py
    @brief      This file is responsible for updating the touch panel object at
                regular intervals and otherwise interfacing with the panel
    @details    This task has a certain period and is run once per period.
                Each time it runs, it requests x, y, and z data from
                the touch panel and uses it to update the shared variables.
    @image      html TouchPanelTask_State_Transition_Diagram.png "Touch Panel Task Finite State Machine" width=960
    @author     Daniel Zevenbergen
    @date       03/16/2022
'''
import utime
from micropython import const
from ulab import numpy as np

def touchpaneltask(touchpanel, ballpos_X, ballpos_Y, ballvel_X, ballvel_Y, ball_z, calibrated, period):
    '''!@brief      This function is responsible for updating and otherwise
                    interfacing with the touch panel object.
        @details    This generator function implements a finite state machine that
                    manages the touch panel object. This includes calibration
                    and regularly updating the position, velocity, and ball_z shared
                    variables.
        @param      touchpanel This parameter is the touchpanel object to be
                    updated and interfaced with.
        @param      ballpos_X This shared variable stores the x position of the ball
        @param      ballpos_Y This shared variable stores the y position of the ball
        @param      ballvel_X This shared variable stores the x velocity of the ball
        @param      ballvel_Y This shared variable stores the y velocity of the ball
        @param      ball_z This shared boolean variable is True when there is contact with the touch panel
        @param      calibrated A boolean variable that reads True when the touch panel
                    is calibrated.
        @param      period This parameter, in microseconds, indicates to the
                    IMU task how often it is supposed to run.
    '''
    S0_INIT = const(0)
    S1_CALIBRATE = const(1)
    S2_UPDATE = const(2)
    
    beta = np.array([[0, 0], [0, 0], [0, 0]])
    beta1 = []
    beta2 = []
    ind = 0
    X = []
    
    state = S0_INIT
    starttime = utime.ticks_us()
    nexttime = starttime
    lasttime = starttime
    while True:
        currenttime = utime.ticks_us()
        if utime.ticks_diff(nexttime, currenttime) <= 0:
            nexttime = utime.ticks_add(nexttime, period)
            
            
            if state == S0_INIT:
                
                try:
                    calfile = open('panel_cal.txt', 'r')
                    calstring = calfile.read()
                    calfile.close()
                    beta = np.array([[0, 0], [0, 0], [0, 0]])
                    calcoefficients = []
                    
                    while len(calstring)>1:
                        commaind = calstring.find(',')
                        substr = calstring[0:commaind]
                        coefficient = float(substr)
                        calcoefficients.append(coefficient)
                        calstring = calstring[commaind+1:]
                        
                    ind = 0
                    for i in range(3):
                        for j in range(2):
                            beta[i][j] = calcoefficients[ind]
                            ind += 1
                            
                    beta1 = []
                    for i in range(3):
                        beta1.append(beta[i][0])
                    beta1 = np.array(beta1)
                    beta2 = []
                    for i in range(3):
                        beta2.append(beta[i][1])
                    beta2 = np.array(beta2)
                    
                    state = S2_UPDATE
                    calibrated.write(True)
                    print('Touch Panel Calibration File Detected!')
                    calibrated.write(True)
                except:
                    state = S1_CALIBRATE
                    X = []
                    ind = 0
                yield None
                
            elif state == S1_CALIBRATE:
                Y = np.array([[-80, -40], [-80, 40], [0, 0], [80, -40], [80, 40]])
                panelread = touchpanel.scan3()
                if panelread[2]:
                    if abs(panelread[0] - Y[ind][0])<15 and abs(panelread[1] - Y[ind][1])<15:
                        X.append([panelread[0], panelread[1], 1])
                        ind +=1
                        if ind == 1:
                            print('Touch upper left corner of panel')
                        elif ind == 2:
                            print('Touch center of panel')
                        elif ind == 3:
                            print('Touch lower right corner of panel')
                        elif ind == 4:
                            print('Touch upper right corner of panel')
                        else:
                            print('All touch panel points taken')
                if ind == 5:
                    X = np.array(X)
                    X_T = X.transpose()
                    X_T_Xinv = np.linalg.inv(np.dot(X_T, X))
                    beta = np.dot(np.dot(X_T_Xinv, X_T), Y)
                    beta1 = []
                    for i in range(3):
                        beta1.append(beta[i][0])
                    beta1 = np.array(beta1)
                    beta2 = []
                    for i in range(3):
                        beta2.append(beta[i][1])
                    beta2 = np.array(beta2)
                    coefstring = ''
                    for i in range(3):
                        for j in range(2):
                            coefstring += str(beta[i][j]) + ','
                            
                    outfile = open('panel_cal.txt', 'w')
                    outfile.write(coefstring)
                    outfile.close()
                    
                    calibrated.write(True)
                    state = S2_UPDATE
                    lasttime = currenttime
                yield None
                
            elif state == S2_UPDATE:
                panelread = touchpanel.scan3()
                ball_z.write(panelread[2])
                X = [panelread[0], panelread[1], 1]
                X = np.array(X)
                Y = [np.dot(X, beta1), np.dot(X, beta2)]
                lastpos_X = ballpos_X.read()
                lastpos_Y = ballpos_Y.read()
                ballpos_X.write(Y[0])
                ballpos_Y.write(Y[1])
                ballvel_X.write((Y[0]-lastpos_X)/(utime.ticks_diff(currenttime, lasttime)/1_000_000))
                ballvel_Y.write((Y[1]-lastpos_Y)/(utime.ticks_diff(currenttime, lasttime)/1_000_000))
                yield None
            lasttime = currenttime
                
        else:
            yield None
                
                
                    
                    
    
    
    
    
    
    
    
    
    
    
    