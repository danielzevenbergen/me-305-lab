'''!@file               ledcontroller.py
    @brief              This file controls an LED on the STM32 Nucleo Board
    @details            This file allows a user to toggle an LED on the STM32
                        Nucleo board between a square wave, sawtooth wave, and
                        sine wave pattern. The brightness of the LED is 
                        controlled as a function of time by pulse width
                        modulation (PWM), and the brightness at a given time is
                        determined by the user-selected waveform. The selected
                        waveform name (square, sawtooth or sine) will also
                        display at the terminal. See source code here: https://bitbucket.org/danielzevenbergen/me-305-lab/src/master/Lab%201/ledcontroller.py
    @htmlonly       <iframe src="https://player.vimeo.com/video/668017801?h=949c1a426f" width="640" height="1138" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                    <p><a href="https://vimeo.com/668017801">Daniel &amp; Matteo Lab 0x01 ME 305</a> from <a href="https://vimeo.com/user164054650">Matteo Gozzini</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
    @endhtmlonly
    @author             Daniel Zevenbergen
    @date               January 19, 2022
'''
import pyb
import utime
import math

pinLED = pyb.Pin(pyb.Pin.cpu.A5) # Create object for LED pin
pinbutton = pyb.Pin(pyb.Pin.cpu.C13) # Create object for button pin

def squarewave(timesincestart):
    '''!@brief          Returns the value of a square wave between 0 and 100, given a time.
        @details        This function takes as its single input the amount of time 
                        that has passed since the arbitrary start time. It returns the
                        value of a square wave (either 0 or 100) with a period of 1 second 
                        at that time.
        @param          timesincestart This parameter is the amount of time that has
                        passed since the arbitrary start time.
        @return         The returned value is the desired brightness of the LED at
                        the given time (either 0 or 100)
    '''
    mod = timesincestart%1000 # This is the modulo of the time since the arbitrary start point (in ms) by 1000
    if mod<500:
        brightness = 100 # led brightness
    else:
        brightness = 0
    return brightness
    
def sinewave(timesincestart):
    '''!@brief          Returns the value of a sine wave between 0 and 100, given a time.
        @details        This function takes as its single input the amount of time 
                        that has passed since the arbitrary start time. It returns the
                        value of a sine wave that varies from 0 to 100 and has a period of 
                        10 seconds at that time.
        @param          timesincestart This parameter is the amount of time that has
                        passed since the arbitrary start time.
        @return         The returned value is the desired brightness of the LED at
                        the given time (between 0 and 100)
    '''
    brightness = 50+50*math.sin(timesincestart*2*math.pi/10000) # led brightness
    return brightness

def sawtoothwave(timesincestart):
    '''!@brief          Returns the value of a sawtooth wave between 0 and 100, given a time.
        @details        This function takes as its single input the amount of time 
                        that has passed since the arbitrary start time. It returns the
                        value of a sawtooth wave that varies from 0 to 100 and has a period 
                        of 1 second at that time.
        @param          timesincestart This parameter is the amount of time that has
                        passed since the arbitrary start time.
        @return         The returned value is the desired brightness of the LED at
                        the given time (between 0 and 100)
    '''
    brightness = (timesincestart%1000)/10 # led brightness
    return brightness

def updatebrightness(dutycycle):
    '''!@brief          Updates the brightness of the LED
        @details        This function takes as its input the desired duty cycle
                        (between 0 and 100) of the LED and updates the brightness of the
                        LED by updating the pulse width percent of timer channel timerch1.
                        It returns no value, only updates the LED brightness.
        @param          dutycycle This parameter represents the desired PWM percentage
                        to be output to the LED.
    '''
    global timerch1
    timerch1.pulse_width_percent(dutycycle)

def onButtonPressFCN(IRQ_src):
    '''!@brief          Updates the value of the button variable when the button interrupt is
                        triggered
        @details        This function is called when the interrupt "ButtonInt" is
                        triggered by the user pressing the button. It takes no parameters
                        and returns no values. It only sets the global variable "button"
                        to "True".
    '''
    global button
    button = True
    
ButtonInt = pyb.ExtInt(pinbutton, mode=pyb.ExtInt.IRQ_FALLING,
                       pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN) # Interrupt that triggers when button is pressed

if __name__ == '__main__':
    button = False # reads True when button has been pressed
    timer = pyb.Timer(2, freq = 20000) # timer object at 20 kHz
    timerch1 = timer.channel(1, pyb.Timer.PWM, pin=pinLED) # timer channel object for led PWM
    starttime = utime.ticks_ms # establishes the time at which the code started running
    state = 0 # The state of the FSM: 0 = startup, 1 = square, 2 = sawtooth, 3 = sine
    print("Press the blue button to cycle through LED patterns")
    while True:
        if state == 0: # startup state
            dutycycle = 100 # PWM duty cycle, ranges from 0-100
            if button:
                state = 1
                button = False
                print("Square Wave Selected")
        elif state == 1: # square wave state
            timesincestart = utime.ticks_diff(utime.ticks_ms(), starttime) # time that has passed since start
            dutycycle = squarewave(timesincestart)
            if button:
                state = 2
                button = False
                print("Sawtooth Wave Selected")
        elif state == 2: #  sawtooth wave state
            timesincestart = utime.ticks_diff(utime.ticks_ms(), starttime)
            dutycycle = sawtoothwave(timesincestart)
            if button:
                state = 3
                button = False
                print("Sine Wave Selected")
        elif state == 3: # sine wave state
            timesincestart = utime.ticks_diff(utime.ticks_ms(), starttime)
            dutycycle = sinewave(timesincestart)
            if button:
                state = 1
                button = False
                print("Square Wave Selected")
        updatebrightness(dutycycle)