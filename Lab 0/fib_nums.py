def fib(idx):
    if idx==0:
        return 0
    elif idx==1:
        return 1
    else:
        fib1 = 0
        fib2 = 1
        for x in range(idx-1):
            temp = fib2
            fib2 = fib1+fib2
            fib1 = temp
        return fib2
if __name__ == '__main__':
    idx = input('Please enter an index: ')
    print(fib(int(idx)))