# The fibcalc function is the backbone of the user interface. It is called when
# this file runs as the main program.
def fibcalc():
    idx = input('Please enter an index: ') # prompt user to enter input
    if not idx.isnumeric(): # check that user input is a positive integer
        print('The index must be a nonnegative integer!')
        quitopt()
    else:
        idx = int(idx)
        print(fib(idx))
        quitopt()
        
        
        
# The quitopt function is called by fibcalc and gives the user the option to
# quit or continue. If the user inputs something other than 'q' or an empty
# string (pressing enter with no input), quitopt calls itself.
def quitopt():
    inp = input('Press enter to continue or input q to quit: ') # prompt user for input
    if inp=='q':
        return '' # exit program if user enters 'q'
    elif inp=='':
        fibcalc() # return to fibcalc if user presses enter with no input
    else:
        quitopt() # re-run quitopt is user inputs anything other than 'q' or presses enter
        
        
        
# The fib function is called by fibcalc. it takes as its input an index and
# returns the fibonnaci number at that index.
def fib(idx):
    if idx==0:
        return 0 # Special case 1: return 0 for index = 0
    elif idx==1:
        return 1 # Special case 2: return 1 for index = 1
    else:
        fib1 = 0
        fib2 = 1
        for x in range(idx-1): # this for loop runs idx-1 times to calculate the fibonnaci number at idx
            temp = fib2
            fib2 = fib1+fib2
            fib1 = temp
        return fib2
    
    
    
if __name__ == '__main__': # conditional to start the UI when this is run from the editor
    fibcalc()