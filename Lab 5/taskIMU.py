'''!@file       taskIMU.py
    @brief      This file is responsible for updating the BNO055 IMU object at
                regular intervals and otherwise interfacing with the IMU
    @details    This task has a certain period and is run once per period.
                Each time it runs, it requests accelerometer and gyro data from
                the IMU and uses it to update the shared variables.
    @image      html IMUTask_State_Transition_Diagram.png "IMU Task Finite State Machine" width=960
    @author     Daniel Zevenbergen
    @date       03/02/2022
'''
import utime
from micropython import const

def IMUtask(BNO055, pos, velocity, rollang, rollvel, pitchang, pitchvel, calibstat, calibrated, period):
    '''!@brief      This function is responsible for updating and otherwise
                    interfacing with the IMU object.
        @details    This generator function implements a finite state machine that
                    manages the BNO055 object. This includes calibration and regularly
                    updating the shared eular angle and gyro velocity variables
                    with values from the BNO055.
        @param      BNO055 This parameter is the BNO055 object to be
                    updated and interfaced with.
        @param      pos This shared variable is used to pass the IMU
                    euler angles from the IMU task to the User task.
        @param      velocity This shared variable is used to pass the Gyroscope
                    velocity from the IMU task to the user task.
        @param      rollang A shared variable containing the roll angle from the
                    BNO055.
        @param      rollvel A shared variable containing the roll velocity from the
                    BNO055.
        @param      pitchang A shared variable containing the pitch angle from the
                    BNO055.
        @param      pitchvel A shared variable containing the pitch velocity from the
                    BNO055.
        @param      calibstat This shared tuple is updated by this task when in the
                    calibration state and can be printed at a regular interval by
                    the user task to notify the user of the calibration status.
        @param      calibrated A boolean variable that reads True when the IMU
                    is calibrated.
        @param      period This parameter, in microseconds, indicates to the
                    IMU task how often it is supposed to run.
'''
    S0_INIT = const(0)
    S1_CALIBRATE = const(1)
    S2_UPDATE = const(2)
    
    state = S0_INIT
    ##@brief    Indicates the time at which the Encoder task was first run
    starttime = utime.ticks_us()
    ##@brief    Indicates the next time at which the Encoder task must run
    nexttime = starttime
    while True:
        currenttime = utime.ticks_us() # indicates current time
        if utime.ticks_diff(nexttime, currenttime) <= 0: # check if it's time to run the encoder task
            nexttime = utime.ticks_add(nexttime, period) # update nexttime
            
            # This set of conditionals represent the various states of the
            # encoder task
            if state == S0_INIT: # state actions
                BNO055.set_mode('NDOF')
                    
                try:
                    calfile = open('IMU_cal_coeffs.txt', 'r')
                    calstring = calfile.read()
                    calfile.close()
                    calcoefficients = []
                    
                    while len(calstring)>1:
                        commaind = calstring.find(',')
                        substr = calstring[0:commaind]
                        coefficient = int(substr, 16)
                        coefficient = hex(coefficient)
                        calcoefficients.append(coefficient)
                        calstring = calstring[commaind+1:]
            
                    BNO055.calcoefficient_write(calcoefficients)
                    state = S2_UPDATE
                    calibrated.write(True)
                    print('Calibration File Detected!')
                except:
                    state = S1_CALIBRATE
                yield None
            elif state == S1_CALIBRATE:
                calibstat.write(BNO055.calstatus())
                if calibstat.read() == (3, 3, 3, 3):
                    
                    # Generate calibration file
                    coef = BNO055.calcoefficient_read()
                    coefstring = ''
                    for num in coef:
                        integer = int.from_bytes(num, "big")
                        hexnum = hex(integer)
                        coefstring += hexnum + ','
                    outfile = open('IMU_cal_coeffs.txt', 'w')
                    outfile.write(coefstring)
                    outfile.close()
                    
                    calibrated.write(True)
                    state = S2_UPDATE
                yield None
            elif state == S2_UPDATE:
                pos.write(BNO055.euler_read())
                velocity.write(BNO055.gyro_read())
               
                pitchang.write(pos.read()[0])
                rollang.write(pos.read()[1])
                pitchvel.write(velocity.read()[0])
                rollvel.write(velocity.read()[1])
                yield None
        else:
            yield None
        