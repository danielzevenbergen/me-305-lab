'''!@file       BNO055.py
    @brief      This file contains a driver class for the Bosch BNO055 IMU
    @author     Daniel Zevenbergen
    @date       02/24/2022
'''

from pyb import I2C
import struct

class BNO055:
    '''!@brief      This class is used to create driver objects for the BNO055
                    IMU.
        @details    This class is used to create BNO055 objects to interface with a
                    Bosch BNO055 IMU. Methods include setting the mode of the IMU,
                    querying the calibration status, reading or writing the IMU's calibration
                    coefficients, reading the euler angles, and reading the velocities from
                    the gyro.
    '''
    
    def __init__(self, I2C):
        '''!@brief      The initializer for the BNO055 object
            @param      i2c An instance of the pyb.I2C class, preconfigured
                        in controller mode.
        '''
        ## An I2C object, preconfigured in controller mode, representing the bus to which the BNO055 is phuysically connected
        self.i2c = I2C
        ## The hexadecimal address of the BNO055 on the I2C bus
        self.adr = 0x28
        ## The hexadecimal memory address that stores the operating mode of the BNO055
        self.OPR_MODE = 0x3D
        ## The hexadecimal memory address that store the calibration status of the BNO055
        self.CALIB_STAT = 0x35
        ## A list used to store the calibration coefficients of the BNO055
        self.CALIB_COEFFICIENTS = []
        
        ## A list of the memory addresses of the calibration data, in the order presented in the BNO055 datasheet.
        self.CALIB_ADDRESSES = [0x6A, 0x69, 0x68, 0x67, 0x66, 0x65, 0x64, 0x63, 0x62, 0x61, 0x60, 0x5F, 0x5E, 0x5D, 0x5C, 0x5B, 0x5A, 0x59, 0x58, 0x57, 0x56, 0x55]
        ## The number of calibration addresses
        self.numcalibaddresses = len(self.CALIB_ADDRESSES)
        
        ## The hexadecimal memory address that stores the MSB of the pitch angle on the BNO055
        self.EUL_pitch_MSB = 0x1F
        ## The hexadecimal memory address that stores the LSB of the pitch angle on the BNO055
        self.EUL_pitch_LSB = 0x1E
        ## The hexadecimal memory address that stores the MSB of the roll angle on the BNO055
        self.EUL_roll_MSB = 0x1D
        ## The hexadecimal memory address that stores the LSB of the roll angle on the BNO055
        self.EUL_roll_LSB = 0x1C
        ## The hexadecimal memory address that stores the MSB of the heading angle on the BNO055
        self.EUL_heading_MSB = 0x1B
        ## The hexadecimal memory address that stores the LSB of the heading angle on the BNO055
        self.EUL_heading_LSB = 0x1A
        
        ## The hexadecimal memory address that stores the MSB of the roll velocity on the BNO055
        self.GYRO_x_MSB = 0x15
        ## The hexadecimal memory address that stores the LSB of the roll velocity on the BNO055
        self.GYRO_x_LSB = 0x14
        ## The hexadecimal memory address that stores the MSB of the pitch velocity on the BNO055
        self.GYRO_y_MSB = 0x17
        ## The hexadecimal memory address that stores the LSB of the pitch velocity on the BNO055
        self.GYRO_y_LSB = 0x16
        ## The hexadecimal memory address that stores the MSB of the heading velocity on the BNO055
        self.GYRO_z_MSB = 0x19
        ## The hexadecimal memory address that stores the LSB of the heading velocity on the BNO055
        self.GYRO_z_LSB = 0x18
        
        
    def set_mode(self, mode):
        '''!@brief      This method sets the mode of the BNO055 to any of the
                        Fusion Modes.
            @param      mode The mode to be switched to
        '''
        modedict = {'CONFIGMODE': 0b0000000, 'IMU': 0b0001000, 'COMPASS': 0b00001001, 'M4G': 0b00001010, 'NDOF_FMC_OFF': 0b00001011, 'NDOF': 0b00001100}
        if mode in set(modedict):
            self.i2c.mem_write(modedict['CONFIGMODE'], self.adr, self.OPR_MODE)
            modecode = modedict[mode]
            self.i2c.mem_write(modecode, self.adr, self.OPR_MODE)
    
    def calstatus(self):
        '''!@brief      This method reads the calibration status from the IMU
                        and parses it.
            @return     Returns a tuple containing the three calibration statuses
        '''
        status = int.from_bytes(self.i2c.mem_read(1, self.adr, self.CALIB_STAT), "big")
        mag_stat = status & 0b00000011
        acc_stat = (status & 0b00001100)>>2
        gyro_stat = (status & 0b00110000)>>4
        sys_stat = (status & 0b11000000)>>6
        return mag_stat, acc_stat, gyro_stat, sys_stat
    
    def calcoefficient_read(self):
        '''!@brief      This method reads the calibration coefficients from the IMU
                        and parses it.
            @return     Returns a tuple containing the calibration coefficients
        '''
        self.CALIB_COEFFICIENTS = []
        
        for memaddress in self.CALIB_ADDRESSES:
            self.CALIB_COEFFICIENTS.append(self.i2c.mem_read(1, self.adr, memaddress))
        return self.CALIB_COEFFICIENTS
    
    def calcoefficient_write(self, coefficients):
        '''!@brief      This method writes the calibration coefficients to the IMU.
            @param      coefficients an array of packed binary data representing the
                        calibration coefficients
        '''
        
        if len(coefficients) == self.numcalibaddresses:
            for memaddress in self.CALIB_ADDRESSES:
                self.i2c.mem_write(1, self.adr, memaddress)
    
    def euler_read(self):
        '''!@brief      This method reads and returns the euler angles from the IMU.
            @return     Returns pitch, roll, and heading euler angles as a tuple
        '''
        data = self.i2c.mem_read(6, self.adr, self.EUL_heading_LSB)
        heading, pitch, roll = struct.unpack("hhh", data)
        heading = -1*heading
        pitch = -1*pitch
        
        return pitch, roll, heading
    
    def gyro_read(self):
        '''!@brief      This method reads and returns the angular velocities from the IMU.
            @return     Returns x, y, and z angular velocities as a tuple
        '''
        data = self.i2c.mem_read(6, self.adr, self.GYRO_x_LSB)
        zdotIMU, ydotIMU, xdotIMU = struct.unpack("hhh", data)
        xdot = ydotIMU
        ydot = -1*zdotIMU
        zdot = xdotIMU
        
        return xdot, ydot, zdot
        
if __name__ == "__main__":
    bus = I2C(1)
    bus.init(I2C.CONTROLLER)
    IMU = BNO055(bus)
    coef = IMU.calcoefficient_read()
    print(coef)
    mag_stat, acc_stat, gyro_stat, sys_stat = IMU.calstatus()
    print(mag_stat, acc_stat, gyro_stat, sys_stat)
    IMU.set_mode('NDOF')
    IMU.calcoefficient_write(coef)
    pitch, roll, heading = IMU.euler_read()
    print(pitch, roll, heading)
    xdot, ydot, zdot = IMU.gyro_read()
    print(xdot, ydot, zdot)
    coef = IMU.calcoefficient_read()
    coefstring = ''
    for num in coef:
        integer = int.from_bytes(num, "big")
        hexnum = hex(integer)
        coefstring += hexnum + ','
    outfile = open('IMU_cal_coeffs.txt', 'w')
    outfile.write(coefstring)
    outfile.close()
        
        
        
        
        
        
        
        
        