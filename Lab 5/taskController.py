'''!@file       taskController.py
    @brief      This task manages the closedloop object controlling the motor
    @details    This file contains a generator function that implements a finite
                state machine to manage a closedloop object to perform closed loop
                control on an abritrary output with an abritrary input.
    @image      html ControllerTask_State_Transition_Diagram.png "Controller Task Finite State Machine" width=960
    @author     Daniel Zevenbergen
    @date       02/07/2022
'''

import utime
from micropython import const

def Ctask(controller, K_p, K_i, K_d, reference, position, velocity, dutycycle, closedloopON, period):
    '''!@brief      This generator function runs at period [period] and manages
                    the ClosedLoop object [controller]
        @details    This generator function implements a finite state machine responsible for
                    managing a closedloop controller object to control the duty cycle
                    of a brushed DC motor based on the angle of the platform. The FSM
                    has two states: inactive and active. When active, it regularly
                    runs the closedloop object and updates the duty cycle of the motor
                    accordingly.
        @param      controller An instance of the closedloop.ClosedLoop class to
                    be managed by this task.
        @param      K_p A shared variable used to pass the proportional gain from
                    the user task to this task.
        @param      K_i A shared variable used to pass the integral gain from
                    the user task to this task.
        @param      K_d A shared variable used to pass the derivative gain from
                    the user task to this task.
        @param      reference A shared variable used to pass the reference value
                    (setpoint) from the user task to this task.
        @param      position A shared variable representing the angle of the platform in one axis.
        @param      velocity A shared variable representing the angular velocity of the platform in one axis.
        @param      dutycycle A shared variable representing the duty cycle of the motor being controlled.
        @param      closedloopON A shared boolean variable indicating whether
                    closed loop control should be on.
        @param      period The period at which this task should run, in microseconds.
    '''
    
    S0_INACTIVE = const(0)
    S1_ACTIVE = const(1)
    
    state = S0_INACTIVE
    
    starttime = utime.ticks_us()
    nexttime = starttime
    while True:
        currenttime = utime.ticks_us()
        if utime.ticks_diff(nexttime, currenttime) <= 0:
            nexttime = utime.ticks_add(currenttime, period)
            
            if state == S0_INACTIVE:
                if closedloopON.read():
                    controller.activate()
                    state = S1_ACTIVE
            
            elif state == S1_ACTIVE:
                controller.set_Kp(K_p.read())
                controller.set_Ki(K_i.read())
                controller.set_Kd(K_d.read())
                controller.set_reference(reference.read())
                dutycycle.write(-1*controller.run(position.read(), velocity.read()))
                if not closedloopON.read():
                    controller.deactivate()
                    state = S0_INACTIVE
                    
            yield None
        else:
            yield None
            
            
            
            
            
            
            