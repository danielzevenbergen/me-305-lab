'''!@file       closedloop.py
    @brief      This file contains the ClosedLoop class, which is used to create
                closed-loop controller objects. This is a multi-purpose object
                and can be used for closed-loop control of any sort.
    @author     Daniel Zevenbergen
    @date       02/17/2022
'''

import utime

class ClosedLoop:
    '''!@brief      This class is used to create controller objects for closed-
                    loop control
        @details
    '''
    def __init__(self, reference_value, sensor_output, sensor_deriv, K_p, K_i, K_d, saturation_low, saturation_high):
        '''!@brief      The constructor creates a closed loop control object
            @details    The constructor takes an inputs the reference value,
                        initial gains K_p, K_i, and K_d, and the saturation
                        limits saturation_low and saturation_high.
            @param      reference_value The reference or target value of the controller
            @param      sensor_output The current output of the sensor
            @param      sensor_deriv The time derivative of the sensor
            @param      K_p The initial proportional gain of the controller
            @param      K_i The initial integral gain of the controller
            @param      K_d The initial derivative gain of the controller
            @param      saturation_low The lower saturation limit of the actuator
            @param      saturation_high The upper saturation limit of the actuator
        '''
        ## The initial reference value to be used for closed-loop control
        self.reference_value = reference_value
        ## The initial proportional gain to be used for closed-loop control
        self.K_p = K_p
        ## The initial integral gain to be used for closed-loop control
        self.K_i = K_i
        ## The initial derivative gain to be used for closed-loop control
        self.K_d = K_d
        ## The lower saturation limit that bounds the controller output
        self.saturation_low = saturation_low
        ## The upper saturation limit that bounds the controller output
        self.saturation_high = saturation_high
        ## The integral or error with respect to time
        self.integral = 0
        ## The error the last time the controller ran
        self.last_error = self.reference_value - sensor_output
        ## The last time the controller ran
        self.last_time = utime.ticks_us()
        ## A boolean variable indicating whether the controller is active
        self.active = False
        
    def run(self, sensor_output, sensor_deriv):
        '''!@brief      Returns the controller output for the given sensor output
            @details    This method should be called regularly to update the
                        integral and derivative values and to update the controller
                        output.
            @return     Returns the controller output
        '''
        self.time = utime.ticks_us()
        deltatime = utime.ticks_diff(self.time, self.last_time)
        self.error = self.reference_value - sensor_output
        
        if self.active:
            self.integral += deltatime/1_000_000*(self.error + self.last_error)/2 # update the integral using trapezoidal integration
            self.derivative = -1*sensor_deriv
        
        # Update last values for use next time
        self.last_time = self.time
        self.last_error = self.error
        
        output = self.K_p*self.error + self.K_i*self.integral + self.K_d*self.derivative
        
        if output < self.saturation_low:
            return self.saturation_low
        elif output > self.saturation_high:
            return self.saturation_high
        else:
            return self.K_p*self.error + self.K_i*self.integral + self.K_d*self.derivative
    
    def activate(self):
        '''!@brief      Activates the closed loop controller
        '''
        self.active = True
        self.last_time = utime.ticks_us()
        
    def deactivate(self):
        '''!@brief      Deactivates the closed loop controller
        '''
        self.active = False
        self.integral = 0
        self.derivative = 0
    
    def set_Kp(self, Kp):
        '''!@brief      Updates The proportional gain value
        '''
        self.K_p = Kp
        
    def set_Ki(self, Ki):
        '''!@brief      Updates The integral gain value
        '''
        self.K_i = Ki
        
    def set_Kd(self, Kd):
        '''!@brief      Updates The derivative gain value
        '''
        self.K_d = Kd
        
    def set_reference(self, reference):
        '''!@brief      Updates the reference value (setpoint)
        '''
        self.reference_value = reference
        
        
        
        