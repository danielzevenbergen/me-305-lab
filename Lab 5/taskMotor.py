'''!@file       taskMotor.py
    @brief      This file interfaces with the motor object to update the duty
                cycle periodically
    @image      html "Lab 3 Motor Task FSM.png" "Motor Task Finite State Machine" width=960
    @author     Daniel Zevenbergen
    @date       02/13/2022
'''

import utime

def Mtask(motor, dutycycle, period):
    '''!@brief      This generator function runs at a period of [period] and
                    updates the motor's duty cycle with the value in the shared
                    variable.
        @details    Every time this function is run, it checks whether it is
                    time to run the motor task again by checking if a time
                    [period] has elapsed since the last time it ran. If it is
                    time to run again, the function updates the duty cycle of
                    motor. Otherwise, the function yields None.
        @param      motor The motor object being controlled by this task
        @param      dutycycle A shared variable representing the desired duty
                    cycle of the motor
        @param      period The period at which this task should run
    '''
    starttime = utime.ticks_us()
    nexttime = starttime
    
    while True:
        currenttime = utime.ticks_us()
        if utime.ticks_diff(nexttime, currenttime) <= 0:
            nexttime = utime.ticks_add(nexttime, period)
            motor.set_duty(dutycycle.read())
            yield None
        else:
            yield None