'''!@file       main.py
    @brief      This file is the main file for Lab 5. It creates a simple
                user interface, IMU object, and two
                motor objects to allow the user to interact with the balancing
                platform, controlled by the two motors, via PuTTy.
    @author     Daniel Zevenbergen
    @date       03/02/2022
'''
import shares
import pyb
from pyb import Timer
import BNO055
import motor
import closedloop
import taskIMU
import taskUser
import taskMotor
import taskData
import taskController
import array
import gc
from pyb import I2C

if __name__ == '__main__':
    pinENCA = pyb.Pin(pyb.Pin.board.PB6)
    pinENCB = pyb.Pin(pyb.Pin.board.PB7)
    timer = pyb.Timer(4, prescaler=0, period=65535)
    timerch1 = timer.channel(1, pyb.Timer.ENC_AB, pin=pinENCA)
    timerch2 = timer.channel(2, pyb.Timer.ENC_AB, pin=pinENCB)
    
    Pin_IN1 = pyb.Pin(pyb.Pin.board.PB4, mode=pyb.Pin.OUT)
    Pin_IN2 = pyb.Pin(pyb.Pin.board.PB5, mode=pyb.Pin.OUT)
    Pin_IN3 = pyb.Pin(pyb.Pin.board.PB0, mode=pyb.Pin.OUT)
    Pin_IN4 = pyb.Pin(pyb.Pin.board.PB1, mode=pyb.Pin.OUT)
    PWM_tim = Timer(3, freq=20000)
    timch1 = PWM_tim.channel(1, Timer.PWM_INVERTED, pin=Pin_IN1)
    timch2 = PWM_tim.channel(2, Timer.PWM_INVERTED, pin=Pin_IN2)
    timch3 = PWM_tim.channel(3, Timer.PWM_INVERTED, pin=Pin_IN3)
    timch4 = PWM_tim.channel(4, Timer.PWM_INVERTED, pin=Pin_IN4)
    
    
    myMotor1 = motor.Motor(PWM_tim, timch1, timch2)
    myMotor2 = motor.Motor(PWM_tim, timch3, timch4)
    datapoints = 901 # number of data points to be recorded by data task
    datarecord = shares.Share(False) # indicates whether data recording is in progress
    
    dataFlag = shares.Share(True)
    pos = shares.Share([0, 0, 0])
    velocity=shares.Share([0, 0, 0])
    rollang = shares.Share(0)
    pitchang = shares.Share(0)
    rollvel = shares.Share(0)
    pitchvel = shares.Share(0)
    timeData = array.array('l', datapoints*[0])
    gc.collect()
    positionData = array.array('l', timeData)
    gc.collect()
    deltaData = array.array('l', timeData)
    timeout = shares.Share(False)
    finalindex = shares.Share(0)
    dutycycle1 = shares.Share(0)
    dutycycle2 = shares.Share(0)
    
    calibstat = shares.Share(0)
    calibrated = shares.Share(False)
    
    K_p = shares.Share(0)
    K_i = shares.Share(0)
    K_d = shares.Share(0)
    referenceX = shares.Share(0)
    referenceY = shares.Share(0)
    closedloopON = shares.Share(False)
    saturationlow = -100
    saturationhigh = 100
    
    M1controller = closedloop.ClosedLoop(referenceY.read(), pos.read()[1], velocity.read()[1], K_p.read(), K_i.read(), K_d.read(), saturationlow, saturationhigh)
    M2controller = closedloop.ClosedLoop(referenceX.read(), pos.read()[2], velocity.read()[2], K_p.read(), K_i.read(), K_d.read(), saturationlow, saturationhigh)
    
    bus = I2C(1)
    bus.init(I2C.CONTROLLER)
    BNO055 = BNO055.BNO055(bus)
    
    Userfreq = 100 # User task frequency in Hz
    IMUFreq = 1000 # Encoder task frequency in Hz
    MotorFreq = 1000 # Motor task frequency in Hz
    DataFreq = 30 # Data task frequency in Hz
    ControllerFreq = 1000 # Controller task frequency in Hz
    Userperiod = 1_000_000//Userfreq # User task period in us
    IMUperiod = 1_000_000//IMUFreq # Encoder task period in us
    Motorperiod = 1_000_000//MotorFreq # Motor task period in us
    Dataperiod = 1_000_000//DataFreq # Data task period in us
    Controllerperiod = 1_000_000//ControllerFreq # Controller task period in us
    serport = pyb.USB_VCP()
    
    task_IMU = taskIMU.IMUtask(BNO055, pos, velocity, rollang, rollvel, pitchang, pitchvel, calibstat, calibrated, IMUperiod)
    task_User = taskUser.Utask(serport, dataFlag, pos, velocity, dutycycle1, dutycycle2, referenceX, K_p, K_i, K_d, closedloopON, datarecord, timeData, positionData, deltaData, timeout, finalindex, calibstat, calibrated, Userperiod)
    task_Motor1 = taskMotor.Mtask(myMotor1, dutycycle1, Motorperiod)
    task_Motor2 = taskMotor.Mtask(myMotor2, dutycycle2, Motorperiod)
    task_Data = taskData.Dtask(datarecord, dataFlag, datapoints, timeData, positionData, deltaData, pos, velocity, timeout, finalindex, Dataperiod)
    task_Controller1 = taskController.Ctask(M1controller, K_p, K_i, K_d, referenceY, rollang, rollvel, dutycycle1, closedloopON, Controllerperiod)
    task_Controller2 = taskController.Ctask(M2controller, K_p, K_i, K_d, referenceX, pitchang, pitchvel, dutycycle2, closedloopON, Controllerperiod)
    tasklist = [task_IMU, task_User, task_Motor1, task_Motor2, task_Data, task_Controller1, task_Controller2]
    
    while True:
        try:
            for task in tasklist:
                next(task) 
        except KeyboardInterrupt:
            break